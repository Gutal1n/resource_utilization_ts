/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return Task; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Resource; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupedTasks; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return Scale; });
var Task = /** @class */ (function () {
    function Task(id, title, startDate, dueDate, assignedTo, complete) {
        this.id = id;
        this.title = title;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.assignedTo = assignedTo;
        this.complete = complete;
    }
    return Task;
}());

var Resource = /** @class */ (function () {
    function Resource(id, title) {
        this.id = id;
        this.title = title;
    }
    return Resource;
}());

var GroupedTasks = /** @class */ (function () {
    function GroupedTasks(name, tasks) {
        this.name = name;
        this.tasks = tasks;
        this.isOpen = true;
    }
    return GroupedTasks;
}());

var Scale;
(function (Scale) {
    Scale["Hours"] = "Hours";
    Scale["QDays"] = "QDays";
    Scale["Days"] = "Days";
    Scale["Weeks"] = "Weeks";
    Scale["ThirdsOfMonths"] = "ThirdsOfMonths";
    Scale["Months"] = "Months";
    Scale["Quarters"] = "Quarters";
    Scale["HalfYears"] = "HalfYears";
})(Scale || (Scale = {}));


/***/ }),
/* 1 */
/***/ (function(module, exports) {

Date.prototype.addDays = function (days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
};
Date.prototype.daysNames = [
    "Sunday", "Monday", "Tuesday", "Wednesday",
    "Thursday", "Friday", "Saturday"
];
Date.prototype.getDayName = function () {
    return this.daysNames[this.getDay()];
};
Date.prototype.monthNames = [
    "January", "February", "March",
    "April", "May", "June",
    "July", "August", "September",
    "October", "November", "December"
];
Date.prototype.getMonthName = function () {
    return this.monthNames[this.getMonth()];
};
Date.prototype.getShortMonthName = function () {
    return this.getMonthName().substr(0, 3);
};
Date.prototype.getArrayDates = function (start, end) {
    var dateArray = new Array();
    var currentDate = start;
    while (currentDate <= end) {
        dateArray.push(new Date(currentDate.getTime()));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
};
Date.prototype.getArrayDatesWithUniqMonth = function (start, end) {
    var arrDates = new Array();
    var arrStr = new Array();
    var currentDate = start;
    while (currentDate <= end) {
        var d = currentDate.getMonthName() + currentDate.getFullYear();
        if (arrStr.indexOf(d) === -1) {
            arrStr.push(d);
            arrDates.push(currentDate);
        }
        currentDate = currentDate.addDays(1);
    }
    return arrDates;
};


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__main__ = __webpack_require__(3);

new __WEBPACK_IMPORTED_MODULE_0__main__["a" /* default */]();


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__syncScroll__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__delay__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__model__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_js_datepicker__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_js_datepicker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_js_datepicker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__table__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__timeline__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__tooltip__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modalWindow__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__dataService__ = __webpack_require__(18);
// import * as Split from '../node_modules/split.js/split';
// import Split from 'split.js';
var Split = __webpack_require__(4);




 /* https://github.com/qodesmith/datepicker */





var Main = /** @class */ (function () {
    function Main() {
        var _this = this;
        this.dataService = new __WEBPACK_IMPORTED_MODULE_9__dataService__["a" /* default */]();
        this.groupedTasks = this.dataService.getGroupedTasks();
        this.dataService.eventObserver.subscribe(this);
        window.addEventListener('load', function () {
            Object(__WEBPACK_IMPORTED_MODULE_0__syncScroll__["a" /* default */])("tableBody", "timelineBody");
            _this.changeScale();
        });
        var leftDiv = document.getElementById("left");
        var rightDiv = document.getElementById("right");
        Split([leftDiv, rightDiv], {
            sizes: [35, 65],
            minSize: 300,
            onDrag: function () {
                var left = document.getElementById('left').offsetWidth;
                _this.table.setWidthContainers(left);
            }
        });
        this.settings = new __WEBPACK_IMPORTED_MODULE_3__settings__["a" /* default */]();
        this.tooltip = new __WEBPACK_IMPORTED_MODULE_7__tooltip__["a" /* default */]();
        this.showDates();
        this.table = new __WEBPACK_IMPORTED_MODULE_5__table__["a" /* default */](this.groupedTasks);
        this.dataService.eventObserver.subscribe(this.table);
        this.timelineHead = document.getElementById('timelineHeader');
        this.timelineBody = document.getElementById('timelineBody');
        this.timeline = new __WEBPACK_IMPORTED_MODULE_6__timeline__["a" /* default */](this.timelineHead, this.timelineBody, this.groupedTasks, this.settings);
        this.dataService.eventObserver.subscribe(this.timeline);
        this.targetDragAndResize = null;
        this.timelineBody.addEventListener("mousedown", this.dragAndResize.bind(this));
        this.timelineBody.addEventListener("mousemove", this.toolTip.bind(this));
        this.timelineBody.addEventListener("mouseleave", function () {
            _this.tooltip.hideTooltip();
        });
        this.modalWindow = new __WEBPACK_IMPORTED_MODULE_8__modalWindow__["a" /* default */](this.dataService);
        this.btnAddNewTask = document.getElementById('btnAddNewTask');
        this.btnAddNewTask.addEventListener('click', this.modalWindow.showAddNewTask.bind(this.modalWindow));
        this.btnAdvancedFilter = document.getElementById('advancedFilter');
        this.btnAdvancedFilter.addEventListener('click', this.modalWindow.showAdvancedFilter.bind(this.modalWindow));
        window.ondblclick = function (e) {
            if (e.target.classList.contains('taskRow')) {
                var taskId = e.target.id.slice(10);
                var task = _this.findTaskByID(taskId);
                console.log(task);
                _this.modalWindow.showUpdateTask(task);
            }
        };
        this.btnScale = document.getElementById('btnScale');
        this.btnScaleHours = document.getElementById('scaleHours');
        this.btnScaleDays = document.getElementById('scaleDays');
        this.btnScaleQuarterDays = document.getElementById('scaleQDays');
        this.btnScaleWeeks = document.getElementById('scaleWeeks');
        this.btnScale3Months = document.getElementById('scale3Months');
        this.btnScaleMonths = document.getElementById('scaleMonths');
        this.btnScaleQuarters = document.getElementById('scaleQuarters');
        this.btnScaleHalfYears = document.getElementById('scaleHalfYears');
        this.btnScaleHours.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleDays.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleQuarterDays.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleWeeks.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScale3Months.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleMonths.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleQuarters.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleHalfYears.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnBack = document.getElementById('btnBack');
        this.btnForward = document.getElementById('btnForward');
        this.btnBack.addEventListener('click', function () {
            _this.timeForward(false);
            _this.showDates();
            _this.timeline.createTimelineHead();
            _this.timeline.createTimelineBody(_this.groupedTasks);
            _this.timeline.createGrid();
        });
        this.btnForward.addEventListener('click', function () {
            _this.timeForward(true);
            _this.showDates();
            _this.timeline.createTimelineHead();
            _this.timeline.createTimelineBody(_this.groupedTasks);
            _this.timeline.createGrid();
        });
        var datepickerForm = document.getElementById('datepickerForm');
        this.datepicker = __WEBPACK_IMPORTED_MODULE_4_js_datepicker__(datepickerForm, {
            dateSelected: this.settings.selectedDate,
            onSelect: function (instance) {
                _this.settings.selectedDate = instance.dateSelected;
                _this.showDates();
                _this.timeline.createTimelineHead();
                _this.timeline.createTimelineBody(_this.groupedTasks);
                _this.timeline.createGrid();
            }
        });
        this.table.tableBody.onmousedown = function (e) {
            var target = e.target.parentElement;
            if (target.classList.contains('resourceTd')) {
                var resourceName = target.id.slice(9);
                var resource = _this.findResourceByName(resourceName);
                resource.isOpen = !resource.isOpen;
                _this.hideOrExpandTasks(resource);
                _this.timeline.createGrid();
            }
        };
        this.timeline.parentElementBody.onmousedown = function (e) {
            var setRigthTarget = function (target) {
                var end = false;
                var newTarget = null;
                while (!end) {
                    if (target.classList.contains('row')) {
                        newTarget = target;
                        end = true;
                    }
                    else if (target.classList.contains('timelineBody')) {
                        end = true;
                    }
                    target = target.parentElement;
                }
                return newTarget;
            };
            var target = setRigthTarget(e.target);
            if (target !== null && target.classList.contains('timeline')) {
                var id = target.id.slice(10);
                var resource = _this.findResourceByName(id);
                resource.isOpen = !resource.isOpen;
                _this.hideOrExpandTasks(resource);
                _this.timeline.createGrid();
            }
        };
        // string highlighting
        this.currentHoverTarget = null;
        this.table.tableBody.onmouseover = function (e) {
            var target = e.target;
            if (_this.currentHoverTarget !== null) {
                _this.currentHoverTarget.classList.remove('rowHover');
            }
            if (target.tagName.toLowerCase() === 'td') {
                _this.currentHoverTarget = document.getElementById('timeline__' + target.parentElement.id.slice(9));
                _this.currentHoverTarget.classList.add('rowHover');
            }
        };
        this.table.tableBody.onmouseleave = function (e) {
            if (_this.currentHoverTarget !== null) {
                _this.currentHoverTarget.classList.remove('rowHover');
                _this.currentHoverTarget = null;
            }
        };
        this.timelineBody.onmouseover = function (e) {
            var target = e.target;
            if (_this.currentHoverTarget !== null) {
                _this.currentHoverTarget.classList.remove('rowHover');
            }
            if (target && target.classList.contains('row')) {
                _this.currentHoverTarget = document.getElementById('tableTr__' + target.id.slice(10));
                _this.currentHoverTarget.classList.add('rowHover');
            }
        };
        this.timelineBody.onmouseleave = function (e) {
            if (_this.currentHoverTarget !== null) {
                _this.currentHoverTarget.classList.remove('rowHover');
                _this.currentHoverTarget = null;
            }
        };
        this.inputFilter = document.getElementById('inputFilter');
        this.inputFilter.onkeyup = function () {
            Object(__WEBPACK_IMPORTED_MODULE_1__delay__["a" /* delay */])(function () {
                _this.filterText = _this.inputFilter.value.toLowerCase();
                _this.dataService.filterTasks(_this.filterText);
                // this.groupedTasks = this.dataService.filterTasks(this.filterText);
                // this.table.createTableBody(this.groupedTasks);
                // this.timeline.createTimelineBody(this.groupedTasks);
                // this.timeline.createGrid();
            }, 500);
        };
    }
    Main.prototype.update = function (data) {
        this.groupedTasks = data;
    };
    Main.prototype.showDates = function () {
        var stDate = document.getElementById('startDate_datepicker');
        var endDate = document.getElementById('endDate_datepicker');
        stDate.innerText = this.settings.startDate.toString().slice(0, 15);
        endDate.innerText = this.settings.endDate.toString().slice(0, 15);
    };
    Main.prototype.dragAndResize = function (e) {
        e.stopPropagation();
        if (e.target.classList.contains('task')) {
            this.targetDragAndResize = e.target;
            this.drag(e);
        }
        else if (e.target.classList.contains('complete')) {
            this.targetDragAndResize = e.target.parentNode;
            this.drag(e);
        }
        else if (e.target.classList.contains('milestone')) {
            this.targetDragAndResize = e.target;
            this.drag(e);
        }
        else if (e.target.classList.contains('completeResize')) {
            this.targetDragAndResize = e.target.parentNode;
            this.resizeComplete(e);
        }
        else if (e.target.classList.contains('taskResize')) {
            this.targetDragAndResize = e.target.parentNode;
            this.resizeTask(e);
        }
    };
    Main.prototype.drag = function (e) {
        var _this = this;
        this.targetDragAndResize.ondragstart = function () { return false; };
        this.tooltip.showMoveTooltip(e);
        var startPos = parseFloat(this.targetDragAndResize.style.left);
        var startMouseX = e.clientX;
        var dx = 0;
        var id = this.getListOfId(this.targetDragAndResize.classList)[0];
        var task = this.findTaskByID(id);
        var previousPos = e.clientX;
        var move = function (e) {
            dx = e.clientX - startMouseX;
            _this.targetDragAndResize.style.left = startPos + dx + 'px';
            _this.shiftOfDateInTask(_this.tooltip.getTask(), e.clientX - previousPos);
            _this.tooltip.showMoveTooltip();
            previousPos = e.clientX;
        };
        document.onmousemove = function (e) {
            move(e);
        };
        document.onmouseup = function (e) {
            _this.shiftOfDateInTask(task, dx);
            _this.table.changeDataInTr(id, task);
            _this.redrawResource(task);
            _this.targetDragAndResize = null;
            document.onmousemove = null;
            document.onmouseup = null;
        };
    };
    Main.prototype.resizeTask = function (e) {
        var _this = this;
        this.targetDragAndResize.ondragstart = function () { return false; };
        var id = this.getListOfId(this.targetDragAndResize.classList)[0];
        var task = this.findTaskByID(id);
        this.tooltip.setTask(task);
        this.tooltip.showResizeTooltip(e);
        var startWidth = parseFloat(this.targetDragAndResize.style.width);
        var startMouseX = e.clientX;
        var dx = 0;
        var previousPos = e.clientX;
        var changeWidthCompleteDiv = function (task) {
            var childs = _this.targetDragAndResize.childNodes;
            var complete = null;
            for (var i = 0; i < childs.length; i++) {
                if (childs[i].classList.contains('complete')) {
                    complete = childs[i];
                    break;
                }
            }
            if (complete) {
                complete.style.width = _this.targetDragAndResize.offsetWidth * task.complete / 100 + 'px';
            }
        };
        var move = function (e) {
            dx = e.clientX - startMouseX;
            if (startWidth + dx < 5) {
                dx = -(startWidth - 5);
            }
            _this.targetDragAndResize.style.width = startWidth + dx + 'px';
            if (startWidth + dx > 5) {
                _this.shiftDueDateInTask(_this.tooltip.getTask(), e.clientX - previousPos);
            }
            _this.tooltip.showResizeTooltip();
            previousPos = e.clientX;
            changeWidthCompleteDiv(task);
        };
        document.onmousemove = function (e) {
            move(e);
        };
        document.onmouseup = function (e) {
            _this.shiftDueDateInTask(task, dx);
            _this.table.changeDataInTr(id, task);
            _this.redrawResource(task);
            _this.targetDragAndResize = null;
            document.onmousemove = null;
            document.onmouseup = null;
        };
    };
    Main.prototype.resizeComplete = function (e) {
        var _this = this;
        this.targetDragAndResize.ondragstart = function () { return false; };
        var id = this.getListOfId(this.targetDragAndResize.parentElement.classList)[0];
        var task = this.findTaskByID(id);
        this.tooltip.setTask(task);
        this.tooltip.showCompliteTooltip(e);
        var startWidth = parseFloat(this.targetDragAndResize.style.width);
        var taskWidth = parseFloat(this.targetDragAndResize.parentElement.style.width);
        var startMouseX = e.clientX;
        var dx = 0;
        var complete = task.complete;
        var move = function (e) {
            dx = e.clientX - startMouseX;
            var newWidth;
            if (startWidth + dx < 0) {
                newWidth = 0;
            }
            else if (startWidth + dx > taskWidth) {
                newWidth = taskWidth;
            }
            else {
                newWidth = startWidth + dx;
            }
            _this.targetDragAndResize.style.width = newWidth + 'px';
            complete = _this.calcComplete(newWidth, taskWidth);
            _this.tooltip.getTask().complete = complete;
            _this.tooltip.showCompliteTooltip();
        };
        document.onmousemove = function (e) {
            move(e);
        };
        document.onmouseup = function (e) {
            _this.changeCompleteInTask(task, complete);
            _this.table.changeDataInTr(id, task);
            _this.targetDragAndResize = null;
            document.onmousemove = null;
            document.onmouseup = null;
        };
    };
    Main.prototype.toolTip = function (e) {
        var _this = this;
        if (this.targetDragAndResize !== null)
            return;
        var target = e.target;
        var listOfId = new Array();
        if (target.classList.contains('task') || target.classList.contains('milestone')) {
            listOfId = this.getListOfId(target.classList);
        }
        else if (target.classList.contains('complete')) {
            listOfId = this.getListOfId(target.parentNode.classList);
        }
        else if (target.classList.contains('resource')) {
            listOfId = this.getListOfId(target.classList);
        }
        else {
            this.tooltip && this.tooltip.hideTooltip();
            return;
        }
        var task;
        if (listOfId.length === 1) {
            var task_1 = this.findTaskByID(listOfId[0]);
            this.tooltip.setTask(task_1);
            this.tooltip.showTaskTooltip(e);
        }
        else if (listOfId.length > 1) {
            var tasks_1 = new Array();
            listOfId.forEach(function (element) {
                tasks_1.push(_this.findTaskByID(element));
            });
            this.tooltip.setTasks(tasks_1);
            this.tooltip.showResourceTooltip(e);
        }
    };
    Main.prototype.getListOfId = function (classList) {
        var listOfId = new Array();
        for (var i = 0; i < classList.length; i++) {
            if (classList[i].indexOf('id__') !== -1)
                listOfId.push(classList[i].toString().slice(4));
        }
        return listOfId;
    };
    Main.prototype.findTaskByID = function (id) {
        var task;
        for (var i = 0; i < this.groupedTasks.length; i++) {
            for (var j = 0; j < this.groupedTasks[i].tasks.length; j++) {
                if (this.groupedTasks[i].tasks[j].id === id) {
                    task = this.groupedTasks[i].tasks[j];
                    return task;
                }
            }
        }
        return task;
    };
    Main.prototype.changeScale = function () {
        this.btnScale.innerText = "Scale: " + this.settings.scale;
    };
    Main.prototype.handlerChangeScale = function (e) {
        this.settings.scale = e.srcElement.getAttribute('data-scale');
        this.changeScale();
        this.showDates();
        this.timeline = new __WEBPACK_IMPORTED_MODULE_6__timeline__["a" /* default */](this.timelineHead, this.timelineBody, this.groupedTasks, this.settings);
        console.log(this.settings);
    };
    Main.prototype.timeForward = function (isForward) {
        var direction = (isForward) ? 1 : -1;
        switch (this.settings.scale) {
            case __WEBPACK_IMPORTED_MODULE_2__model__["c" /* Scale */].Hours:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000 * 60 * 60 * 24 * 3);
                break;
            case __WEBPACK_IMPORTED_MODULE_2__model__["c" /* Scale */].Days:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000 * 60 * 60 * 24 * 80);
                break;
            case __WEBPACK_IMPORTED_MODULE_2__model__["c" /* Scale */].QDays:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000 * 60 * 60 * 24 * 8);
                break;
            case __WEBPACK_IMPORTED_MODULE_2__model__["c" /* Scale */].Weeks:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000 * 60 * 60 * 24 * 112);
                break;
            case __WEBPACK_IMPORTED_MODULE_2__model__["c" /* Scale */].ThirdsOfMonths:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000 * 60 * 60 * 24 * 200);
                break;
            case __WEBPACK_IMPORTED_MODULE_2__model__["c" /* Scale */].Months:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000 * 60 * 60 * 24 * 650);
                break;
            case __WEBPACK_IMPORTED_MODULE_2__model__["c" /* Scale */].Quarters:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000 * 60 * 60 * 24 * 2000);
                break;
            case __WEBPACK_IMPORTED_MODULE_2__model__["c" /* Scale */].HalfYears:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000 * 60 * 60 * 24 * 7300);
                break;
        }
        this.datepicker.dateSelected = this.settings.selectedDate;
        this.datepicker.setDate(new Date(this.settings.selectedDate.toString()));
    };
    Main.prototype.shiftOfDateInTask = function (task, pixelShift) {
        var dateShift = pixelShift * this.settings.timePerPixel;
        task.startDate = new Date(task.startDate.getTime() + dateShift);
        task.dueDate = new Date(task.dueDate.getTime() + dateShift);
        return task;
    };
    Main.prototype.shiftDueDateInTask = function (task, pixelShift) {
        var dateShift = pixelShift * this.settings.timePerPixel;
        task.dueDate = new Date(task.dueDate.getTime() + dateShift);
        return task;
    };
    Main.prototype.changeCompleteInTask = function (task, newComplete) {
        task.complete = newComplete;
    };
    Main.prototype.calcComplete = function (widthComplete, widthTask) {
        var res = 100 * widthComplete / widthTask;
        if (res < 0.4)
            return 0;
        return Math.ceil(res);
    };
    Main.prototype.findAllTasksOfResousceByTask = function (task) {
        for (var i = 0; i < this.groupedTasks.length; i++) {
            if (this.groupedTasks[i].name === task.assignedTo.id) {
                return this.groupedTasks[i];
            }
        }
    };
    Main.prototype.findRowOfResource = function (nameRes) {
        return document.getElementById('timeline__' + nameRes);
    };
    Main.prototype.redrawResource = function (task) {
        var res = this.findAllTasksOfResousceByTask(task);
        var row = this.findRowOfResource(res.name);
        row.innerHTML = '';
        this.timeline.drawResource(row, res);
    };
    Main.prototype.findResourceByName = function (name) {
        for (var i = 0; i < this.groupedTasks.length; i++) {
            if (name === this.groupedTasks[i].name)
                return this.groupedTasks[i];
        }
    };
    Main.prototype.hideOrExpandTasks = function (resource) {
        for (var i = 0; i < resource.tasks.length; i++) {
            var idTr = "tableTr__" + resource.tasks[i].id;
            var idTimeline = "timeline__" + resource.tasks[i].id;
            var element = document.getElementById(idTr);
            var elementTimeline = document.getElementById(idTimeline);
            if (resource.isOpen) {
                element.style.display = 'table-row';
                elementTimeline.style.display = 'block';
            }
            else {
                element.style.display = 'none';
                elementTimeline.style.display = 'none';
            }
        }
    };
    return Main;
}());
/* harmony default export */ __webpack_exports__["a"] = (Main);


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

/*! Split.js - v1.3.5 */

(function (global, factory) {
	 true ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.Split = factory());
}(this, (function () { 'use strict';

// The programming goals of Split.js are to deliver readable, understandable and
// maintainable code, while at the same time manually optimizing for tiny minified file size,
// browser compatibility without additional requirements, graceful fallback (IE8 is supported)
// and very few assumptions about the user's page layout.
var global = window;
var document = global.document;

// Save a couple long function names that are used frequently.
// This optimization saves around 400 bytes.
var addEventListener = 'addEventListener';
var removeEventListener = 'removeEventListener';
var getBoundingClientRect = 'getBoundingClientRect';
var NOOP = function () { return false; };

// Figure out if we're in IE8 or not. IE8 will still render correctly,
// but will be static instead of draggable.
var isIE8 = global.attachEvent && !global[addEventListener];

// This library only needs two helper functions:
//
// The first determines which prefixes of CSS calc we need.
// We only need to do this once on startup, when this anonymous function is called.
//
// Tests -webkit, -moz and -o prefixes. Modified from StackOverflow:
// http://stackoverflow.com/questions/16625140/js-feature-detection-to-detect-the-usage-of-webkit-calc-over-calc/16625167#16625167
var calc = (['', '-webkit-', '-moz-', '-o-'].filter(function (prefix) {
    var el = document.createElement('div');
    el.style.cssText = "width:" + prefix + "calc(9px)";

    return (!!el.style.length)
}).shift()) + "calc";

// The second helper function allows elements and string selectors to be used
// interchangeably. In either case an element is returned. This allows us to
// do `Split([elem1, elem2])` as well as `Split(['#id1', '#id2'])`.
var elementOrSelector = function (el) {
    if (typeof el === 'string' || el instanceof String) {
        return document.querySelector(el)
    }

    return el
};

// The main function to initialize a split. Split.js thinks about each pair
// of elements as an independant pair. Dragging the gutter between two elements
// only changes the dimensions of elements in that pair. This is key to understanding
// how the following functions operate, since each function is bound to a pair.
//
// A pair object is shaped like this:
//
// {
//     a: DOM element,
//     b: DOM element,
//     aMin: Number,
//     bMin: Number,
//     dragging: Boolean,
//     parent: DOM element,
//     isFirst: Boolean,
//     isLast: Boolean,
//     direction: 'horizontal' | 'vertical'
// }
//
// The basic sequence:
//
// 1. Set defaults to something sane. `options` doesn't have to be passed at all.
// 2. Initialize a bunch of strings based on the direction we're splitting.
//    A lot of the behavior in the rest of the library is paramatized down to
//    rely on CSS strings and classes.
// 3. Define the dragging helper functions, and a few helpers to go with them.
// 4. Loop through the elements while pairing them off. Every pair gets an
//    `pair` object, a gutter, and special isFirst/isLast properties.
// 5. Actually size the pair elements, insert gutters and attach event listeners.
var Split = function (ids, options) {
    if ( options === void 0 ) options = {};

    var dimension;
    var clientDimension;
    var clientAxis;
    var position;
    var paddingA;
    var paddingB;
    var elements;

    // All DOM elements in the split should have a common parent. We can grab
    // the first elements parent and hope users read the docs because the
    // behavior will be whacky otherwise.
    var parent = elementOrSelector(ids[0]).parentNode;
    var parentFlexDirection = global.getComputedStyle(parent).flexDirection;

    // Set default options.sizes to equal percentages of the parent element.
    var sizes = options.sizes || ids.map(function () { return 100 / ids.length; });

    // Standardize minSize to an array if it isn't already. This allows minSize
    // to be passed as a number.
    var minSize = options.minSize !== undefined ? options.minSize : 100;
    var minSizes = Array.isArray(minSize) ? minSize : ids.map(function () { return minSize; });
    var gutterSize = options.gutterSize !== undefined ? options.gutterSize : 10;
    var snapOffset = options.snapOffset !== undefined ? options.snapOffset : 30;
    var direction = options.direction || 'horizontal';
    var cursor = options.cursor || (direction === 'horizontal' ? 'ew-resize' : 'ns-resize');
    var gutter = options.gutter || (function (i, gutterDirection) {
        var gut = document.createElement('div');
        gut.className = "gutter gutter-" + gutterDirection;
        return gut
    });
    var elementStyle = options.elementStyle || (function (dim, size, gutSize) {
        var style = {};

        if (typeof size !== 'string' && !(size instanceof String)) {
            if (!isIE8) {
                style[dim] = calc + "(" + size + "% - " + gutSize + "px)";
            } else {
                style[dim] = size + "%";
            }
        } else {
            style[dim] = size;
        }

        return style
    });
    var gutterStyle = options.gutterStyle || (function (dim, gutSize) { return (( obj = {}, obj[dim] = (gutSize + "px"), obj ))
        var obj; });

    // 2. Initialize a bunch of strings based on the direction we're splitting.
    // A lot of the behavior in the rest of the library is paramatized down to
    // rely on CSS strings and classes.
    if (direction === 'horizontal') {
        dimension = 'width';
        clientDimension = 'clientWidth';
        clientAxis = 'clientX';
        position = 'left';
        paddingA = 'paddingLeft';
        paddingB = 'paddingRight';
    } else if (direction === 'vertical') {
        dimension = 'height';
        clientDimension = 'clientHeight';
        clientAxis = 'clientY';
        position = 'top';
        paddingA = 'paddingTop';
        paddingB = 'paddingBottom';
    }

    // 3. Define the dragging helper functions, and a few helpers to go with them.
    // Each helper is bound to a pair object that contains it's metadata. This
    // also makes it easy to store references to listeners that that will be
    // added and removed.
    //
    // Even though there are no other functions contained in them, aliasing
    // this to self saves 50 bytes or so since it's used so frequently.
    //
    // The pair object saves metadata like dragging state, position and
    // event listener references.

    function setElementSize (el, size, gutSize) {
        // Split.js allows setting sizes via numbers (ideally), or if you must,
        // by string, like '300px'. This is less than ideal, because it breaks
        // the fluid layout that `calc(% - px)` provides. You're on your own if you do that,
        // make sure you calculate the gutter size by hand.
        var style = elementStyle(dimension, size, gutSize);

        // eslint-disable-next-line no-param-reassign
        Object.keys(style).forEach(function (prop) { return (el.style[prop] = style[prop]); });
    }

    function setGutterSize (gutterElement, gutSize) {
        var style = gutterStyle(dimension, gutSize);

        // eslint-disable-next-line no-param-reassign
        Object.keys(style).forEach(function (prop) { return (gutterElement.style[prop] = style[prop]); });
    }

    // Actually adjust the size of elements `a` and `b` to `offset` while dragging.
    // calc is used to allow calc(percentage + gutterpx) on the whole split instance,
    // which allows the viewport to be resized without additional logic.
    // Element a's size is the same as offset. b's size is total size - a size.
    // Both sizes are calculated from the initial parent percentage,
    // then the gutter size is subtracted.
    function adjust (offset) {
        var a = elements[this.a];
        var b = elements[this.b];
        var percentage = a.size + b.size;

        a.size = (offset / this.size) * percentage;
        b.size = (percentage - ((offset / this.size) * percentage));

        setElementSize(a.element, a.size, this.aGutterSize);
        setElementSize(b.element, b.size, this.bGutterSize);
    }

    // drag, where all the magic happens. The logic is really quite simple:
    //
    // 1. Ignore if the pair is not dragging.
    // 2. Get the offset of the event.
    // 3. Snap offset to min if within snappable range (within min + snapOffset).
    // 4. Actually adjust each element in the pair to offset.
    //
    // ---------------------------------------------------------------------
    // |    | <- a.minSize               ||              b.minSize -> |    |
    // |    |  | <- this.snapOffset      ||     this.snapOffset -> |  |    |
    // |    |  |                         ||                        |  |    |
    // |    |  |                         ||                        |  |    |
    // ---------------------------------------------------------------------
    // | <- this.start                                        this.size -> |
    function drag (e) {
        var offset;

        if (!this.dragging) { return }

        // Get the offset of the event from the first side of the
        // pair `this.start`. Supports touch events, but not multitouch, so only the first
        // finger `touches[0]` is counted.
        if ('touches' in e) {
            offset = e.touches[0][clientAxis] - this.start;
        } else {
            offset = e[clientAxis] - this.start;
        }

        // If within snapOffset of min or max, set offset to min or max.
        // snapOffset buffers a.minSize and b.minSize, so logic is opposite for both.
        // Include the appropriate gutter sizes to prevent overflows.
        if (offset <= elements[this.a].minSize + snapOffset + this.aGutterSize) {
            offset = elements[this.a].minSize + this.aGutterSize;
        } else if (offset >= this.size - (elements[this.b].minSize + snapOffset + this.bGutterSize)) {
            offset = this.size - (elements[this.b].minSize + this.bGutterSize);
        }

        // Actually adjust the size.
        adjust.call(this, offset);

        // Call the drag callback continously. Don't do anything too intensive
        // in this callback.
        if (options.onDrag) {
            options.onDrag();
        }
    }

    // Cache some important sizes when drag starts, so we don't have to do that
    // continously:
    //
    // `size`: The total size of the pair. First + second + first gutter + second gutter.
    // `start`: The leading side of the first element.
    //
    // ------------------------------------------------
    // |      aGutterSize -> |||                      |
    // |                     |||                      |
    // |                     |||                      |
    // |                     ||| <- bGutterSize       |
    // ------------------------------------------------
    // | <- start                             size -> |
    function calculateSizes () {
        // Figure out the parent size minus padding.
        var a = elements[this.a].element;
        var b = elements[this.b].element;

        this.size = a[getBoundingClientRect]()[dimension] + b[getBoundingClientRect]()[dimension] + this.aGutterSize + this.bGutterSize;
        this.start = a[getBoundingClientRect]()[position];
    }

    // stopDragging is very similar to startDragging in reverse.
    function stopDragging () {
        var self = this;
        var a = elements[self.a].element;
        var b = elements[self.b].element;

        if (self.dragging && options.onDragEnd) {
            options.onDragEnd();
        }

        self.dragging = false;

        // Remove the stored event listeners. This is why we store them.
        global[removeEventListener]('mouseup', self.stop);
        global[removeEventListener]('touchend', self.stop);
        global[removeEventListener]('touchcancel', self.stop);

        self.parent[removeEventListener]('mousemove', self.move);
        self.parent[removeEventListener]('touchmove', self.move);

        // Delete them once they are removed. I think this makes a difference
        // in memory usage with a lot of splits on one page. But I don't know for sure.
        delete self.stop;
        delete self.move;

        a[removeEventListener]('selectstart', NOOP);
        a[removeEventListener]('dragstart', NOOP);
        b[removeEventListener]('selectstart', NOOP);
        b[removeEventListener]('dragstart', NOOP);

        a.style.userSelect = '';
        a.style.webkitUserSelect = '';
        a.style.MozUserSelect = '';
        a.style.pointerEvents = '';

        b.style.userSelect = '';
        b.style.webkitUserSelect = '';
        b.style.MozUserSelect = '';
        b.style.pointerEvents = '';

        self.gutter.style.cursor = '';
        self.parent.style.cursor = '';
    }

    // startDragging calls `calculateSizes` to store the inital size in the pair object.
    // It also adds event listeners for mouse/touch events,
    // and prevents selection while dragging so avoid the selecting text.
    function startDragging (e) {
        // Alias frequently used variables to save space. 200 bytes.
        var self = this;
        var a = elements[self.a].element;
        var b = elements[self.b].element;

        // Call the onDragStart callback.
        if (!self.dragging && options.onDragStart) {
            options.onDragStart();
        }

        // Don't actually drag the element. We emulate that in the drag function.
        e.preventDefault();

        // Set the dragging property of the pair object.
        self.dragging = true;

        // Create two event listeners bound to the same pair object and store
        // them in the pair object.
        self.move = drag.bind(self);
        self.stop = stopDragging.bind(self);

        // All the binding. `window` gets the stop events in case we drag out of the elements.
        global[addEventListener]('mouseup', self.stop);
        global[addEventListener]('touchend', self.stop);
        global[addEventListener]('touchcancel', self.stop);

        self.parent[addEventListener]('mousemove', self.move);
        self.parent[addEventListener]('touchmove', self.move);

        // Disable selection. Disable!
        a[addEventListener]('selectstart', NOOP);
        a[addEventListener]('dragstart', NOOP);
        b[addEventListener]('selectstart', NOOP);
        b[addEventListener]('dragstart', NOOP);

        a.style.userSelect = 'none';
        a.style.webkitUserSelect = 'none';
        a.style.MozUserSelect = 'none';
        a.style.pointerEvents = 'none';

        b.style.userSelect = 'none';
        b.style.webkitUserSelect = 'none';
        b.style.MozUserSelect = 'none';
        b.style.pointerEvents = 'none';

        // Set the cursor, both on the gutter and the parent element.
        // Doing only a, b and gutter causes flickering.
        self.gutter.style.cursor = cursor;
        self.parent.style.cursor = cursor;

        // Cache the initial sizes of the pair.
        calculateSizes.call(self);
    }

    // 5. Create pair and element objects. Each pair has an index reference to
    // elements `a` and `b` of the pair (first and second elements).
    // Loop through the elements while pairing them off. Every pair gets a
    // `pair` object, a gutter, and isFirst/isLast properties.
    //
    // Basic logic:
    //
    // - Starting with the second element `i > 0`, create `pair` objects with
    //   `a = i - 1` and `b = i`
    // - Set gutter sizes based on the _pair_ being first/last. The first and last
    //   pair have gutterSize / 2, since they only have one half gutter, and not two.
    // - Create gutter elements and add event listeners.
    // - Set the size of the elements, minus the gutter sizes.
    //
    // -----------------------------------------------------------------------
    // |     i=0     |         i=1         |        i=2       |      i=3     |
    // |             |       isFirst       |                  |     isLast   |
    // |           pair 0                pair 1             pair 2           |
    // |             |                     |                  |              |
    // -----------------------------------------------------------------------
    var pairs = [];
    elements = ids.map(function (id, i) {
        // Create the element object.
        var element = {
            element: elementOrSelector(id),
            size: sizes[i],
            minSize: minSizes[i],
        };

        var pair;

        if (i > 0) {
            // Create the pair object with it's metadata.
            pair = {
                a: i - 1,
                b: i,
                dragging: false,
                isFirst: (i === 1),
                isLast: (i === ids.length - 1),
                direction: direction,
                parent: parent,
            };

            // For first and last pairs, first and last gutter width is half.
            pair.aGutterSize = gutterSize;
            pair.bGutterSize = gutterSize;

            if (pair.isFirst) {
                pair.aGutterSize = gutterSize / 2;
            }

            if (pair.isLast) {
                pair.bGutterSize = gutterSize / 2;
            }

            // if the parent has a reverse flex-direction, switch the pair elements.
            if (parentFlexDirection === 'row-reverse' || parentFlexDirection === 'column-reverse') {
                var temp = pair.a;
                pair.a = pair.b;
                pair.b = temp;
            }
        }

        // Determine the size of the current element. IE8 is supported by
        // staticly assigning sizes without draggable gutters. Assigns a string
        // to `size`.
        //
        // IE9 and above
        if (!isIE8) {
            // Create gutter elements for each pair.
            if (i > 0) {
                var gutterElement = gutter(i, direction);
                setGutterSize(gutterElement, gutterSize);

                gutterElement[addEventListener]('mousedown', startDragging.bind(pair));
                gutterElement[addEventListener]('touchstart', startDragging.bind(pair));

                parent.insertBefore(gutterElement, element.element);

                pair.gutter = gutterElement;
            }
        }

        // Set the element size to our determined size.
        // Half-size gutters for first and last elements.
        if (i === 0 || i === ids.length - 1) {
            setElementSize(element.element, element.size, gutterSize / 2);
        } else {
            setElementSize(element.element, element.size, gutterSize);
        }

        var computedSize = element.element[getBoundingClientRect]()[dimension];

        if (computedSize < element.minSize) {
            element.minSize = computedSize;
        }

        // After the first iteration, and we have a pair object, append it to the
        // list of pairs.
        if (i > 0) {
            pairs.push(pair);
        }

        return element
    });

    function setSizes (newSizes) {
        newSizes.forEach(function (newSize, i) {
            if (i > 0) {
                var pair = pairs[i - 1];
                var a = elements[pair.a];
                var b = elements[pair.b];

                a.size = newSizes[i - 1];
                b.size = newSize;

                setElementSize(a.element, a.size, pair.aGutterSize);
                setElementSize(b.element, b.size, pair.bGutterSize);
            }
        });
    }

    function destroy () {
        pairs.forEach(function (pair) {
            pair.parent.removeChild(pair.gutter);
            elements[pair.a].element.style[dimension] = '';
            elements[pair.b].element.style[dimension] = '';
        });
    }

    if (isIE8) {
        return {
            setSizes: setSizes,
            destroy: destroy,
        }
    }

    return {
        setSizes: setSizes,
        getSizes: function getSizes () {
            return elements.map(function (element) { return element.size; })
        },
        collapse: function collapse (i) {
            if (i === pairs.length) {
                var pair = pairs[i - 1];

                calculateSizes.call(pair);

                if (!isIE8) {
                    adjust.call(pair, pair.size - pair.bGutterSize);
                }
            } else {
                var pair$1 = pairs[i];

                calculateSizes.call(pair$1);

                if (!isIE8) {
                    adjust.call(pair$1, pair$1.aGutterSize);
                }
            }
        },
        destroy: destroy,
    }
};

return Split;

})));


/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = syncScroll;
function syncScroll(firstContainer, secondContainer) {
    var isSyncingLeftScroll = false;
    var isSyncingRightScroll = false;
    var left = document.getElementById(firstContainer);
    var right = document.getElementById(secondContainer);
    left.addEventListener("scroll", function () {
        if (!isSyncingLeftScroll) {
            isSyncingRightScroll = true;
            right.scrollTop = this.scrollTop;
        }
        isSyncingLeftScroll = false;
    });
    right.addEventListener("scroll", function () {
        if (!isSyncingRightScroll) {
            isSyncingLeftScroll = true;
            left.scrollTop = this.scrollTop;
        }
        isSyncingRightScroll = false;
    });
}


/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__model__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__date_extension__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__date_extension___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__date_extension__);


var DAY_MS = 86400000; //1000*60*60*24;
var Settings = /** @class */ (function () {
    function Settings() {
        this._scale = __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Days;
        this._selectedDate = new Date();
        this.setStartAndEndDatesOfScale(this.selectedDate);
        this.widthCell = 30;
        this.calcWidthCanvas();
        this.calcutaleTimePerPixel();
    }
    Object.defineProperty(Settings.prototype, "scale", {
        get: function () {
            return this._scale;
        },
        set: function (scale) {
            this._scale = scale;
            this.setStartAndEndDatesOfScale(this._selectedDate || new Date());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Settings.prototype, "selectedDate", {
        get: function () {
            return this._selectedDate;
        },
        set: function (date) {
            this._selectedDate = date;
            this.setStartAndEndDatesOfScale(date);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Settings.prototype, "widthCanvas", {
        get: function () {
            this.calcWidthCanvas();
            return this._widthCanvas;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Settings.prototype, "timePerPixel", {
        get: function () {
            this.calcutaleTimePerPixel();
            return this._timePerPixel;
        },
        enumerable: true,
        configurable: true
    });
    Settings.prototype.setStartAndEndDatesOfScale = function (selectedDate) {
        switch (this.scale) {
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Hours:
                this.startDate = new Date(selectedDate.getTime() - DAY_MS * 2);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS * 2);
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Days:
                this.startDate = new Date(selectedDate.getTime() - DAY_MS * 40);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS * 40);
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].QDays:
                this.startDate = new Date(selectedDate.getTime() - DAY_MS * 8);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS * 8);
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Weeks:
                this.startDate = new Date(selectedDate.getTime() - DAY_MS * 112);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS * 112);
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].ThirdsOfMonths:
                this.startDate = new Date(selectedDate.getTime() - DAY_MS * 200);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS * 200);
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Months:
                this.startDate = new Date(selectedDate.getTime() - DAY_MS * 650);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS * 650);
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Quarters:
                this.startDate = new Date(selectedDate.getTime() - DAY_MS * 2000);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS * 2000);
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].HalfYears:
                this.startDate = new Date(selectedDate.getTime() - DAY_MS * 7300);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS * 7300);
                break;
        }
        this.startDate.setHours(0, 0, 0, 0);
        this.endDate.setHours(23, 59, 59, 999);
    };
    Settings.prototype.calcWidthCanvas = function () {
        switch (this.scale) {
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Hours:
                this._widthCanvas = this.widthCell * 24 * Date.prototype.getArrayDates(this.startDate, this.endDate).length;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Days:
                this._widthCanvas = this.widthCell * Date.prototype.getArrayDates(this.startDate, this.endDate).length;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].QDays:
                this._widthCanvas = this.widthCell * 4 * Date.prototype.getArrayDates(this.startDate, this.endDate).length;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Weeks:
                this._widthCanvas = 2 * this.widthCell * Math.ceil(Date.prototype.getArrayDates(this.startDate, this.endDate).length / 7);
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].ThirdsOfMonths:
                this._widthCanvas = 2 * this.widthCell * Math.ceil(Date.prototype.getArrayDates(this.startDate, this.endDate).length / 10);
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Months:
                this._widthCanvas = 2 * this.widthCell * Math.ceil(Date.prototype.getArrayDates(this.startDate, this.endDate).length / 30);
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Quarters:
                this._widthCanvas = 2 * this.widthCell * Math.ceil(Date.prototype.getArrayDates(this.startDate, this.endDate).length / 90);
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].HalfYears:
                this._widthCanvas = this.widthCell * Math.ceil(Date.prototype.getArrayDates(this.startDate, this.endDate).length / 180);
                break;
        }
    };
    Settings.prototype.calcutaleTimePerPixel = function () {
        this._timePerPixel = this.deltaTime(this.startDate, this.endDate) / this._widthCanvas;
    };
    Settings.prototype.deltaTime = function (start, end) {
        return end.getTime() - start.getTime();
    };
    return Settings;
}());
/* harmony default export */ __webpack_exports__["a"] = (Settings);


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;!function(t,e){var n="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t};"object"===( false?"undefined":n(exports))?module.exports=e(): true?!(__WEBPACK_AMD_DEFINE_RESULT__ = (function(){return e()}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)):t.datepicker=e()}(this,function(){"use strict";function t(t,o){var a=t.split?document.querySelector(t):t;o=e(o||n(),a,t);var s=a.parentElement,i=document.createElement("div"),c=o,u=c.startDate,d=c.dateSelected,p=a===document.body||a===document.querySelector("html"),y={el:a,parent:s,nonInput:"INPUT"!==a.nodeName,noPosition:p,position:!p&&o.position,startDate:u,dateSelected:d,minDate:o.minDate,maxDate:o.maxDate,noWeekends:!!o.noWeekends,calendar:i,currentMonth:(u||d).getMonth(),currentMonthName:(o.months||g)[(u||d).getMonth()],currentYear:(u||d).getFullYear(),setDate:h,reset:f,remove:v,onSelect:o.onSelect,onShow:o.onShow,onHide:o.onHide,onMonthChange:o.onMonthChange,formatter:o.formatter,months:o.months||g,days:o.customDays||S,startDay:o.startDay,overlayPlaceholder:o.overlayPlaceholder||"4-digit year",overlayButton:o.overlayButton||"Submit",disableMobile:o.disableMobile,isMobile:"ontouchstart"in window};return d&&l(a,y),i.classList.add("qs-datepicker"),i.classList.add("qs-hidden"),w.push(a),r(u||d,y),b.forEach(function(t){window.addEventListener(t,D.bind(y))}),"static"===getComputedStyle(s).position&&(s.style.position="relative"),s.appendChild(i),y}function e(t,e){if(w.includes(e))throw new Error("A datepicker already exists on that element.");var n=t.position,r=t.maxDate,a=t.minDate,s=t.dateSelected,i=t.formatter,c=t.customMonths,l=t.customDays,u=t.overlayPlaceholder,d=t.overlayButton,h=t.startDay;if(n){if(!["tr","tl","br","bl"].some(function(t){return n===t}))throw new Error('"options.position" must be one of the following: tl, tr, bl, or br.');t.position=o(n)}else t.position=o("bl");if(["startDate","dateSelected","minDate","maxDate"].forEach(function(e){if(t[e]){if(!p(t[e])||isNaN(+t[e]))throw new TypeError('"options.'+e+'" needs to be a valid JavaScript Date object.');t[e]=y(t[e])}}),t.startDate=t.startDate||t.dateSelected||y(new Date),t.formatter="function"==typeof i?i:null,r<a)throw new Error('"maxDate" in options is less than "minDate".');if(s){if(a>s)throw new Error('"dateSelected" in options is less than "minDate".');if(r<s)throw new Error('"dateSelected" in options is greater than "maxDate".')}if(["onSelect","onShow","onHide","onMonthChange"].forEach(function(e){t[e]="function"==typeof t[e]&&t[e]}),[c,l].forEach(function(e,n){if(e){var o=['"customMonths" must be an array with 12 strings.','"customDays" must be an array with 7 strings.'];if("[object Array]"!=={}.toString.call(e)||e.length!==(n?7:12))throw new Error(o[n]);t[n?"days":"months"]=e}}),void 0!==h&&+h&&+h>0&&+h<7){var f=(t.customDays||S).slice(),v=f.splice(0,h);t.customDays=f.concat(v),t.startDay=+h}else t.startDay=0;return[u,d].forEach(function(e,n){e&&e.split&&(n?t.overlayButton=e:t.overlayPlaceholder=e)}),t}function n(){return{startDate:y(new Date),position:"bl"}}function o(t){var e={};return e[M[t[0]]]=1,e[M[t[1]]]=1,e}function r(t,e){var n=a(t,e),o=s(t,e),r=i(e);e.calendar.innerHTML=n+o+r}function a(t,e){return'\n      <div class="qs-controls">\n        <div class="qs-arrow qs-left"></div>\n        <div class="qs-month-year">\n          <span class="qs-month">'+e.months[t.getMonth()]+'</span>\n          <span class="qs-year">'+t.getFullYear()+'</span>\n        </div>\n        <div class="qs-arrow qs-right"></div>\n      </div>\n    '}function s(t,e){var n=e.minDate,o=e.maxDate,r=e.dateSelected,a=e.currentYear,s=e.currentMonth,i=e.noWeekends,c=e.days,l=new Date,u=l.toJSON().slice(0,7)===t.toJSON().slice(0,7),d=new Date(new Date(t).setDate(1)),h=d.getDay()-e.startDay,f=h<0?7:0;d.setMonth(d.getMonth()+1),d.setDate(0);var p=d.getDate(),y=[],v=f+7*((h+p)/7|0);v+=(h+p)%7?7:0,0!==e.startDay&&0===h&&(v+=7);for(var m=1;m<=v;m++){var q=c[(m-1)%7],D=m-(h>=0?h:7+h),w=new Date(a,s,D),b=D<1||D>p,S="",g='<span class="qs-num">'+D+"</span>";if(b)S="qs-empty",g="";else{var M=n&&w<n||o&&w>o,E=c[6],L=c[0],N=q===E||q===L,x=u&&!M&&D===l.getDate();M=M||i&&N,S=M?"qs-disabled":x?"qs-current":""}+w!=+r||b||(S+=" qs-active"),y.push('<div class="qs-square qs-num '+q+" "+S+'">'+g+"</div>")}var C=c.map(function(t){return'<div class="qs-square qs-day">'+t+"</div>"}).concat(y);if(C.length%7!=0){throw new Error("Calendar not constructed properly. The # of squares should be a multiple of 7.")}return C.unshift('<div class="qs-squares">'),C.push("</div>"),C.join("")}function i(t){return'\n      <div class="qs-overlay qs-hidden">\n        <div class="qs-close">&#10005;</div>\n        <input type="number" class="qs-overlay-year" placeholder="'+t.overlayPlaceholder+'" />\n        <div class="qs-submit qs-disabled">'+t.overlayButton+"</div>\n      </div>\n    "}function c(t,e){var n=e.currentMonth,o=e.currentYear,r=e.calendar,a=e.el,s=e.onSelect,i=r.querySelector(".qs-active"),c=t.textContent;e.dateSelected=new Date(o,n,c),i&&i.classList.remove("qs-active"),t.classList.add("qs-active"),l(a,e),m(e),s&&s(e)}function l(t,e){if(!e.nonInput)return e.formatter?e.formatter(t,e.dateSelected):void(t.value=e.dateSelected.toDateString())}function u(t,e,n){n?e.currentYear=n:(e.currentMonth+=t.contains("qs-right")?1:-1,12===e.currentMonth?(e.currentMonth=0,e.currentYear++):-1===e.currentMonth&&(e.currentMonth=11,e.currentYear--)),r(new Date(e.currentYear,e.currentMonth,1),e),e.currentMonthName=e.months[e.currentMonth],e.onMonthChange&&n&&e.onMonthChange(e)}function d(t){if(!t.noPosition){var e=t.el,n=t.calendar,o=t.position,r=t.parent,a=o.top,s=o.right,i=r.getBoundingClientRect(),c=e.getBoundingClientRect(),l=n.getBoundingClientRect(),u=c.top-i.top+r.scrollTop,d="\n      top:"+(u-(a?l.height:-1*c.height))+"px;\n      left:"+(c.left-i.left+(s?c.width-l.width:0))+"px;\n    ";n.setAttribute("style",d)}}function h(t,e){if(!p(t))throw new TypeError("`setDate` needs a JavaScript Date object.");t=y(t),this.currentYear=t.getFullYear(),this.currentMonth=t.getMonth(),this.currentMonthName=this.months[t.getMonth()],this.dateSelected=e?void 0:t,!e&&l(this.el,this),r(t,this),e&&(this.el.value="")}function f(){this.setDate(this.startDate,!0)}function p(t){return"[object Date]"==={}.toString.call(t)}function y(t){return new Date(t.toDateString())}function v(){var t=this.calendar,e=this.parent,n=this.el;b.forEach(function(t){window.removeEventListener(t,D)}),t.remove(),t.hasOwnProperty("parentStyle")&&(e.style.position=""),w=w.filter(function(t){return t!==n})}function m(t){t.calendar.classList.add("qs-hidden"),t.onHide&&t.onHide(t)}function q(t){t.calendar.classList.remove("qs-hidden"),d(t),t.onShow&&t.onShow(t)}function D(t){function e(e){var r=e.calendar,a=l.classList,s=r.querySelector(".qs-month-year"),d=a.contains("qs-close");if(a.contains("qs-num")){var h="SPAN"===l.nodeName?l.parentNode:l;!["qs-disabled","qs-active","qs-empty"].some(function(t){return h.classList.contains(t)})&&c(h,e)}else if(a.contains("qs-arrow"))u(a,e);else if(i.includes(s)||d)n(r,d,e);else if(l.classList.contains("qs-submit")){var f=r.querySelector(".qs-overlay-year");o(t,f,e)}}function n(t,e,n){[".qs-overlay",".qs-controls",".qs-squares"].forEach(function(e,n){t.querySelector(e).classList.toggle(n?"qs-blur":"qs-hidden")});var o=t.querySelector(".qs-overlay-year");e?o.value="":o.focus()}function o(t,e,n){var o=isNaN((new Date).setFullYear(e.value||void 0));if(13===t.which||"click"===t.type){if(o||e.classList.contains("qs-disabled"))return;u(null,n,e.value)}else{n.calendar.querySelector(".qs-submit").classList[o?"add":"remove"]("qs-disabled")}}if(!this.isMobile||!this.disableMobile){if(!t.path){for(var r=t.target,a=[];r!==document;)a.push(r),r=r.parentNode;t.path=a}var s=t.type,i=t.path,l=t.target,d=this.calendar,h=this.el,f=d.classList,p=f.contains("qs-hidden"),y=i.includes(d);if("keydown"===s){var v=d.querySelector(".qs-overlay");if(13===t.which&&!v.classList.contains("qs-hidden"))return t.stopPropagation(),o(t,l,this);if(27===t.which)return n(d,!0,this);if(9!==t.which)return}if("focusin"===s)return l===h&&q(this);this.noPosition?y?e(this):p?q(this):m(this):p?l===h&&q(this):"click"===s&&y?e(this):"input"===s?o(t,l,this):l!==h&&m(this)}}Array.prototype.includes||(Array.prototype.includes=function(t){var e=!1;return this.forEach(function(n){n===t&&(e=!0)}),e});var w=[],b=["click","focusin","keydown","input"],S=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],g=["January","February","March","April","May","June","July","August","September","October","November","December"],M={t:"top",r:"right",b:"bottom",l:"left"};return t});

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var COLUMN_NAMES = [
    "Id",
    "Title",
    "Start Date",
    "Due Date",
    "Complete"
];
var COLUMN_LENGHT = 6;
var COLUMN_WIDTH = 150;
var Table = /** @class */ (function () {
    function Table(groupedTasks) {
        this.tableHead = document.getElementById('tableHead');
        this.tableBody = document.getElementById('scrollTableBody');
        this.fillColumnNames(COLUMN_NAMES);
        this.setWidthContainers(this.columnNames.length * 150);
        this.createTableHead();
        this.createTableBody(groupedTasks);
    }
    Table.prototype.update = function (data) {
        console.log('update table: ', data);
        this.createTableBody(data);
    };
    Table.prototype.setWidthContainers = function (width) {
        if (width < this.columnNames.length * COLUMN_WIDTH) {
            width = this.columnNames.length * COLUMN_WIDTH;
        }
        document.getElementById('tableHead').style.width = width + 'px';
        document.getElementById('tableBody').style.width = width + 'px';
    };
    Table.prototype.fillColumnNames = function (columnNames) {
        this.columnNames = columnNames;
        this.columnNames.push(' ');
    };
    // createTableHead(groupedTasks: GroupedTasks[]) {
    Table.prototype.createTableHead = function () {
        var table = document.createElement('table');
        table.className = 'tableHead';
        var thead = document.createElement("thead");
        table.appendChild(thead);
        var tr = document.createElement('tr');
        thead.appendChild(tr);
        for (var i = 0; i < this.columnNames.length; i++) {
            var th = document.createElement('th');
            th.innerText = this.columnNames[i];
            th.className = "th__" + i;
            if (i === this.columnNames.length - 1) {
                th.classList.add('last');
            }
            tr.appendChild(th);
        }
        // let obj = groupedTasks[0].tasks[0];
        // for (let key in obj) {
        //     if (key==="assignedTo") continue;
        //     let th = document.createElement('th');
        //     key = (key.charAt(0).toUpperCase() + key.slice(1)).split(/(?=[A-Z])/).join(' ');
        //     th.innerText = key;
        //     tr.appendChild(th);
        // }
        this.tableHead.appendChild(table);
    };
    Table.prototype.createTableBody = function (groupedTasks) {
        this.tableBody.innerHTML = '';
        var tbody = document.createElement('tbody');
        for (var i = 0; i < groupedTasks.length; i++) {
            var resouseTr = document.createElement("tr");
            resouseTr.className = 'resourceTd';
            resouseTr.id = 'tableTr__' + groupedTasks[i].name;
            var resTd = document.createElement('td');
            resTd.colSpan = COLUMN_LENGHT;
            resTd.innerText = "(" + groupedTasks[i].tasks.length + ") " + groupedTasks[i].name;
            resouseTr.appendChild(resTd);
            tbody.appendChild(resouseTr);
            for (var j = 0; j < groupedTasks[i].tasks.length; j++) {
                var taskTr = document.createElement('tr');
                taskTr.id = 'tableTr__' + groupedTasks[i].tasks[j].id;
                var idTd = document.createElement('td');
                idTd.innerText = groupedTasks[i].tasks[j].id;
                idTd.id = 'id__' + groupedTasks[i].tasks[j].id;
                var titleTd = document.createElement('td');
                titleTd.innerText = groupedTasks[i].tasks[j].title;
                titleTd.id = 'title__' + groupedTasks[i].tasks[j].id;
                var startTd = document.createElement('td');
                startTd.innerText = groupedTasks[i].tasks[j].startDate.toString().slice(0, 25);
                startTd.id = 'startDate__' + groupedTasks[i].tasks[j].id;
                var dueTd = document.createElement('td');
                dueTd.innerText = groupedTasks[i].tasks[j].dueDate.toString().slice(0, 25);
                dueTd.id = 'dueDate__' + groupedTasks[i].tasks[j].id;
                var completeTd = document.createElement('td');
                completeTd.innerText = groupedTasks[i].tasks[j].complete.toString() + ' %';
                completeTd.id = 'complete__' + groupedTasks[i].tasks[j].id;
                var empty = document.createElement('td');
                empty.classList.add('last');
                taskTr.appendChild(idTd);
                taskTr.appendChild(titleTd);
                taskTr.appendChild(startTd);
                taskTr.appendChild(dueTd);
                taskTr.appendChild(completeTd);
                taskTr.appendChild(empty);
                tbody.appendChild(taskTr);
                if (!groupedTasks[i].isOpen) {
                    taskTr.style.display = 'none';
                }
                ;
            }
        }
        this.tableBody.appendChild(tbody);
        return true;
    };
    Table.prototype.changeDataInTr = function (idTr, task) {
        var elements = document.getElementById('tableTr__' + idTr).childNodes;
        for (var i = 0; i < elements.length; i++) {
            var id = elements[i].id;
            var field = id.slice(0, id.indexOf('_'));
            for (var obj in task) {
                if (field === obj) {
                    if (task[obj] instanceof Date) {
                        elements[i].innerHTML = task[obj].toString().slice(0, 25);
                    }
                    else {
                        elements[i].innerHTML = task[obj];
                    }
                }
            }
        }
    };
    return Table;
}());
/* harmony default export */ __webpack_exports__["a"] = (Table);


/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__model__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__date_extension__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__date_extension___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__date_extension__);


var WEEK_CELLS = 7;
var DAY_CELLS = 24;
var QDAY_CELLS = 4;
var HEIGHT_ROW = 25;
var Matrix = /** @class */ (function () {
    function Matrix() {
        this.count = 0;
        this.tasks = new Array();
    }
    return Matrix;
}());
var Timeline = /** @class */ (function () {
    function Timeline(parentElementHeader, parentElementBody, groupedTasks, settings) {
        this.parentElementHeader = parentElementHeader;
        this.parentElementBody = parentElementBody;
        this.settings = settings;
        this.createTimelineHead();
        this.createTimelineBody(groupedTasks);
        this.createGrid();
    }
    Timeline.prototype.update = function (data) {
        console.log('update timeline: ', data);
        this.createTimelineBody(data);
        this.createGrid();
    };
    Timeline.prototype.createGrid = function () {
        this.clearGrid();
        this.drawCurrentTimeLine();
        this.drawGridTimeline();
        return true;
    };
    Timeline.prototype.createTimelineHead = function () {
        this.parentElementHeader.style.width = this.settings.widthCanvas + 'px';
        this.parentElementHeader.innerHTML = '';
        switch (this.settings.scale) {
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Hours:
                this.drawHeaderOfHoursScale();
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Days:
                this.drawHeaderOfDaysScale();
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].QDays:
                this.drawHeaderOfQuaterDays();
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Weeks:
                this.drawHeaderOfWeeks();
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].ThirdsOfMonths:
                this.drawHeaderOfThirdsOfMonths();
        }
    };
    Timeline.prototype.createTimelineBody = function (groupedTasks) {
        this.parentElementBody.style.width = this.settings.widthCanvas + 'px';
        this.parentElementBody.innerHTML = '';
        this.drawTasks(groupedTasks);
        return true;
    };
    Timeline.prototype.drawResource = function (parentRow, groupedTasks) {
        var matrix = this.calculateMatrixForDrawingResource(groupedTasks);
        for (var i = 0; i < matrix.length; i++) {
            if (matrix[i].count !== 0) {
                var start = i;
                var width = 1;
                i++;
                while (matrix[i] && matrix[i].count === matrix[start].count) {
                    width++;
                    i++;
                }
                var div = document.createElement('div');
                div.style.width = width + 'px';
                div.style.left = start + 'px';
                div.className = 'resource';
                div.innerText = matrix[start].tasks.length === 1 ? matrix[start].tasks[0].title : matrix[start].tasks.length + " Appointments";
                this.colorIdentifierOfTask(div, matrix[start]);
                parentRow.appendChild(div);
            }
        }
    };
    Timeline.prototype.colorIdentifierOfTask = function (el, matrix) {
        var countTasks = matrix.count;
        if (countTasks === 1)
            el.classList.add('green');
        else if (countTasks === 2)
            el.classList.add('orange');
        else if (countTasks > 2 && countTasks <= 4)
            el.classList.add('red');
        else if (countTasks > 4)
            el.classList.add('darkRed');
        for (var i = 0; i < matrix.tasks.length; i++) {
            el.classList.add('id__' + matrix.tasks[i].id);
        }
    };
    Timeline.prototype.drawTasks = function (groupedTasks) {
        for (var i = 0; i < groupedTasks.length; i++) {
            var row = document.createElement('div');
            row.classList.add('row');
            row.classList.add('timeline');
            row.id = 'timeline__' + groupedTasks[i].name;
            row.style.width = this.settings.widthCanvas + 'px';
            this.parentElementBody.appendChild(row);
            this.drawResource(row, groupedTasks[i]);
            for (var j = 0; j < groupedTasks[i].tasks.length; j++) {
                var row_1 = document.createElement('div');
                row_1.classList.add('row');
                row_1.classList.add('taskRow');
                row_1.id = 'timeline__' + groupedTasks[i].tasks[j].id;
                row_1.style.width = this.settings.widthCanvas + 'px';
                this.parentElementBody.appendChild(row_1);
                if (groupedTasks[i].tasks[j].startDate.getTime() !== groupedTasks[i].tasks[j].dueDate.getTime()) {
                    this.drawTask(row_1, groupedTasks[i].tasks[j]);
                }
                else {
                    this.drawMilestone(row_1, groupedTasks[i].tasks[j]);
                }
            }
        }
    };
    Timeline.prototype.drawMilestone = function (parentElement, task) {
        var milestone = document.createElement('div');
        milestone.className = 'milestone';
        milestone.classList.add('id__' + task.id);
        this.setLeftShift(milestone, task);
        parentElement.appendChild(milestone);
    };
    Timeline.prototype.drawTask = function (parentElement, task) {
        var taskElement = document.createElement('div');
        taskElement.className = 'task';
        taskElement.classList.add('id__' + task.id);
        this.setWidthTask(taskElement, task);
        this.setLeftShift(taskElement, task);
        parentElement.appendChild(taskElement);
        var taskResize = document.createElement('div');
        taskResize.className = 'taskResize';
        taskElement.appendChild(taskResize);
        var complete = document.createElement('div');
        complete.className = 'complete';
        this.setComplete(complete, taskElement, task);
        taskElement.appendChild(complete);
        var completeResize = document.createElement('div');
        completeResize.className = 'completeResize';
        complete.appendChild(completeResize);
    };
    Timeline.prototype.setWidthTask = function (element, task) {
        var width = (task.dueDate.getTime() - task.startDate.getTime()) / this.settings.timePerPixel;
        element.style.width = width + 'px';
    };
    Timeline.prototype.setLeftShift = function (element, task) {
        var startPos = (task.startDate.getTime() - this.settings.startDate.getTime()) / this.settings.timePerPixel;
        element.style.left = startPos + 'px';
    };
    Timeline.prototype.setComplete = function (completeElem, taskElem, taskData) {
        var width = taskData.complete * parseInt(taskElem.style.width) / 100;
        completeElem.style.width = width + 'px';
    };
    Timeline.prototype.drawHeaderOfHoursScale = function () {
        var dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        for (var i = 0; i < dates.length; i++) {
            var day = document.createElement('div');
            day.style.width = DAY_CELLS * this.settings.widthCell + 'px';
            day.style.height = this.parentElementHeader.offsetHeight + 'px';
            day.style.left = i * DAY_CELLS * this.settings.widthCell + 'px';
            day.classList.add("headerCell");
            this.parentElementHeader.appendChild(day);
            var top_1 = document.createElement('div');
            top_1.innerText = dates[i].toString().slice(4, 15);
            top_1.style.height = day.offsetHeight / 2 + 'px';
            top_1.classList.add("headerTopCell");
            day.appendChild(top_1);
            var bottom = document.createElement('div');
            bottom.style.height = day.offsetHeight / 2 + 'px';
            day.appendChild(bottom);
            for (var i_1 = 0; i_1 < DAY_CELLS; i_1++) {
                var hour = document.createElement('div');
                hour.innerText = i_1.toString();
                hour.style.width = this.settings.widthCell + 'px';
                hour.style.height = day.offsetHeight / 2 + 'px';
                hour.classList.add("headerBottomCell");
                bottom.appendChild(hour);
            }
        }
    };
    Timeline.prototype.drawHeaderOfDaysScale = function () {
        var dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        var arr = this.splitOnWeekArrays(dates);
        var leftShift = 0;
        for (var i = 0; i < arr.length; i++) {
            var week = document.createElement('div');
            week.style.width = arr[i].length * this.settings.widthCell + 'px';
            week.style.height = this.parentElementHeader.offsetHeight + 'px';
            week.style.left = leftShift * this.settings.widthCell + 'px';
            week.classList.add("headerCell");
            leftShift += arr[i].length;
            this.parentElementHeader.appendChild(week);
            var top_2 = document.createElement('div');
            top_2.innerText = (arr[i][0]).toString().slice(4, 15);
            top_2.style.height = week.offsetHeight / 2 + 'px';
            top_2.classList.add("headerTopCell");
            week.appendChild(top_2);
            var bottom = document.createElement('div');
            bottom.style.height = week.offsetHeight / 2 + 'px';
            week.appendChild(bottom);
            var days = this.getOnlyDayArray(arr[i]);
            for (var i_2 = 0; i_2 < days.length; i_2++) {
                var day = document.createElement('div');
                day.innerText = days[i_2].toString();
                day.style.width = this.settings.widthCell + 'px';
                day.style.height = week.offsetHeight / 2 + 'px';
                day.classList.add("headerBottomCell");
                bottom.appendChild(day);
            }
        }
    };
    Timeline.prototype.drawHeaderOfQuaterDays = function () {
        var dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        for (var i = 0; i < dates.length; i++) {
            var day = document.createElement('div');
            day.style.width = QDAY_CELLS * this.settings.widthCell + 'px';
            day.style.height = this.parentElementHeader.offsetHeight + 'px';
            day.style.left = i * QDAY_CELLS * this.settings.widthCell + 'px';
            day.classList.add("headerCell");
            this.parentElementHeader.appendChild(day);
            var top_3 = document.createElement('div');
            top_3.innerText = (dates[i]).toString().slice(4, 15);
            top_3.style.height = day.offsetHeight / 2 + 'px';
            top_3.classList.add("headerTopCell");
            day.appendChild(top_3);
            var bottom = document.createElement('div');
            bottom.style.height = day.offsetHeight / 2 + 'px';
            day.appendChild(bottom);
            for (var i_3 = 0; i_3 < QDAY_CELLS; i_3++) {
                var hour = document.createElement('div');
                hour.innerText = (i_3 * 6).toString();
                hour.style.width = this.settings.widthCell + 'px';
                hour.style.height = day.offsetHeight / 2 + 'px';
                hour.classList.add("headerBottomCell");
                bottom.appendChild(hour);
            }
        }
    };
    Timeline.prototype.drawHeaderOfWeeks = function () {
    };
    Timeline.prototype.drawHeaderOfThirdsOfMonths = function () {
        var leftShift = 0;
        var dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        var months = this.splitOnMonthsArrays(dates);
        var WIDTH_DAY = this.parentElementHeader.offsetWidth / dates.length;
        var _loop_1 = function (i) {
            var month = document.createElement('div');
            var widthMonth = WIDTH_DAY * months[i].length;
            month.style.width = widthMonth + 'px';
            month.style.height = this_1.parentElementHeader.offsetHeight + 'px';
            month.style.left = leftShift + 'px';
            leftShift += widthMonth;
            month.classList.add("headerCell");
            this_1.parentElementHeader.appendChild(month);
            var top_4 = document.createElement('div');
            top_4.innerText = months[i][0].getMonthName() + ' ' + months[i][0].getFullYear();
            top_4.style.height = month.offsetHeight / 2 + 'px';
            top_4.classList.add("headerTopCell");
            month.appendChild(top_4);
            var bottom = document.createElement('div');
            bottom.style.height = month.offsetHeight / 2 + 'px';
            month.appendChild(bottom);
            var drawCell = function (widthCells) {
                for (var j = 0; j < widthCells.length; j++) {
                    if (widthCells[j] === 0)
                        continue;
                    var cell = document.createElement('div');
                    cell.style.width = widthCells[j] + 'px';
                    cell.style.height = month.offsetHeight / 2 + 'px';
                    if (widthCells[j] > 0) {
                        cell.innerText = (j === 0 || j === 1) ? ((j === 0) ? "S" : "M") : "E";
                    }
                    cell.classList.add("headerBottomCell");
                    bottom.appendChild(cell);
                }
            };
            if (i !== 0 && i !== months.length - 1) {
                var w = [widthMonth / 3, widthMonth / 3, widthMonth / 3];
                drawCell(w);
            }
            else if (i === 0) {
                var w3 = (widthMonth - WIDTH_DAY * 10 > 0) ? WIDTH_DAY * 10 : widthMonth;
                var w2 = (widthMonth - w3 - WIDTH_DAY * 10 > 0) ? WIDTH_DAY * 10 : widthMonth - w3;
                var w1 = widthMonth - w3 - w2;
                var w = [w1, w2, w3];
                drawCell(w);
            }
            else if (i === months.length - 1) {
                var w1 = (widthMonth - WIDTH_DAY * 10 > 0) ? WIDTH_DAY * 10 : widthMonth;
                var w2 = (widthMonth - w1 - WIDTH_DAY * 10 > 0) ? WIDTH_DAY * 10 : widthMonth - w1;
                var w3 = widthMonth - w1 - w2;
                var w = [w1, w2, w3];
                drawCell(w);
            }
        };
        var this_1 = this;
        for (var i = 0; i < months.length; i++) {
            _loop_1(i);
        }
    };
    Timeline.prototype.splitOnWeekArrays = function (dates) {
        var arr = new Array();
        var weekArr = new Array();
        while (dates.length > 0) {
            weekArr.push(dates[0]);
            if (dates[0].getDay() == 6) {
                arr.push(weekArr);
                weekArr = new Array();
            }
            dates.shift();
        }
        if (weekArr.length > 0)
            arr.push(weekArr);
        return arr;
    };
    Timeline.prototype.splitOnMonthsArrays = function (dates) {
        var arr = new Array();
        var monthArr = new Array();
        monthArr.push(dates[0]);
        for (var i = 1; i < dates.length; i++) {
            var str = dates[i].getMonthName() + dates[i].getFullYear();
            var previous = dates[i - 1].getMonthName() + dates[i - 1].getFullYear();
            if (str === previous) {
                monthArr.push(dates[i]);
            }
            else {
                arr.push(monthArr);
                monthArr = new Array();
                monthArr.push(dates[i]);
            }
            if (i === dates.length - 1 && monthArr.length > 0) {
                arr.push(monthArr);
            }
        }
        return arr;
    };
    Timeline.prototype.getOnlyDayArray = function (arr) {
        var dateArray = new Array();
        for (var i = 0; i < arr.length; i++) {
            dateArray[i] = arr[i].getDate();
        }
        return dateArray;
    };
    Timeline.prototype.calculateMatrixForDrawingResource = function (groupedTasksByResource) {
        var matrix = new Array(this.settings.widthCanvas);
        for (var i = 0; i < matrix.length; i++) {
            matrix[i] = new Matrix();
        }
        for (var i = 0; i < groupedTasksByResource.tasks.length; i++) {
            if (this.checkOverlapDatesWithScale(groupedTasksByResource.tasks[i].startDate, groupedTasksByResource.tasks[i].dueDate)) {
                var startPos = Math.floor((groupedTasksByResource.tasks[i].startDate.getTime() - this.settings.startDate.getTime()) / this.settings.timePerPixel);
                var widthRect = Math.ceil((groupedTasksByResource.tasks[i].dueDate.getTime() - groupedTasksByResource.tasks[i].startDate.getTime()) / this.settings.timePerPixel);
                if (startPos < 0) {
                    widthRect = startPos + widthRect;
                    startPos = 0;
                }
                if (startPos + widthRect > this.settings.widthCanvas) {
                    widthRect = this.settings.widthCanvas - startPos;
                }
                for (var j = startPos; j < startPos + widthRect; j++) {
                    matrix[j].count++;
                    matrix[j].tasks.push(groupedTasksByResource.tasks[i]);
                }
            }
        }
        return matrix;
    };
    Timeline.prototype.checkOverlapDatesWithScale = function (start, end) {
        return this.settings.startDate.getTime() < end.getTime() &&
            start.getTime() < this.settings.endDate.getTime();
    };
    Timeline.prototype.checkOverlapDateWithScale = function (date) {
        return this.settings.startDate.getTime() < date.getTime() &&
            date.getTime() < this.settings.endDate.getTime();
    };
    Timeline.prototype.drawCurrentTimeLine = function () {
        var today = new Date();
        if (!this.checkOverlapDateWithScale(today))
            return;
        today.setHours(0, 0, 0, 0);
        var x = (today.getTime() - this.settings.startDate.getTime()) / this.settings.timePerPixel;
        var line = document.createElement('div');
        line.style.height = this.calcCountRow() * HEIGHT_ROW + 'px';
        line.style.left = x + 'px';
        line.className = 'currentTimeline';
        this.parentElementBody.appendChild(line);
    };
    Timeline.prototype.drawGridTimeline = function () {
        switch (this.settings.scale) {
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Hours:
                this.gridOfHoursScale();
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Days:
                this.gridOfDaysScale();
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].QDays:
                this.gridOfQDaysScale();
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].Weeks:
                this.gridOfWeeksScale();
                break;
            case __WEBPACK_IMPORTED_MODULE_0__model__["c" /* Scale */].ThirdsOfMonths:
                this.gridOfThirdsOfMonths();
                break;
        }
    };
    Timeline.prototype.gridOfQDaysScale = function () {
        var height = this.calcCountRow() * HEIGHT_ROW;
        for (var i = 0; i < this.parentElementBody.offsetWidth; i += QDAY_CELLS * this.settings.widthCell) {
            var line = document.createElement('div');
            line.style.height = height + 'px';
            line.style.left = i + 'px';
            line.className = 'gridLine';
            this.parentElementBody.appendChild(line);
        }
    };
    Timeline.prototype.gridOfWeeksScale = function () {
    };
    Timeline.prototype.gridOfHoursScale = function () {
        var height = this.calcCountRow() * HEIGHT_ROW;
        for (var i = 0; i < this.parentElementBody.offsetWidth; i += DAY_CELLS * this.settings.widthCell) {
            var line = document.createElement('div');
            line.style.height = height + 'px';
            line.style.left = i + 'px';
            line.className = 'gridLine';
            this.parentElementBody.appendChild(line);
        }
    };
    Timeline.prototype.gridOfDaysScale = function () {
        var height = this.calcCountRow() * HEIGHT_ROW;
        var leftShift = 0;
        var dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        var arr = this.splitOnWeekArrays(dates);
        for (var i = 0; i < arr.length; i++) {
            var line = document.createElement('div');
            line.style.height = height + 'px';
            line.style.left = leftShift + 'px';
            line.className = 'gridLine';
            this.parentElementBody.appendChild(line);
            leftShift += this.settings.widthCell * arr[i].length;
        }
    };
    Timeline.prototype.gridOfThirdsOfMonths = function () {
        var height = this.calcCountRow() * HEIGHT_ROW;
        var dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        var months = this.splitOnMonthsArrays(dates);
        var WIDTH_DAY = this.parentElementHeader.offsetWidth / dates.length;
        var leftShift = 0;
        for (var i = 0; i < months.length; i++) {
            var line = document.createElement('div');
            line.style.height = height + 'px';
            line.style.left = leftShift + 'px';
            line.className = 'gridLine';
            this.parentElementBody.appendChild(line);
            leftShift += WIDTH_DAY * months[i].length;
        }
    };
    Timeline.prototype.calcCountRow = function () {
        var count = 0;
        var elements = this.parentElementBody.childNodes;
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].classList.contains('row') && elements[i].style.display !== 'none') {
                count++;
            }
        }
        return count;
    };
    Timeline.prototype.clearGrid = function () {
        var grid = document.getElementsByClassName('gridLine');
        var currentLine = document.getElementsByClassName('currentTimeline')[0];
        if (currentLine) {
            var parentCurLine = currentLine.parentElement;
            parentCurLine.removeChild(currentLine);
        }
        for (var i = grid.length; i > 0; i--) {
            var parentGrid = grid[0].parentElement;
            parentGrid.removeChild(grid[0]);
        }
    };
    return Timeline;
}());
/* harmony default export */ __webpack_exports__["a"] = (Timeline);


/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var assign = __webpack_require__(11).assign;
var Tooltip = /** @class */ (function () {
    function Tooltip() {
        this.tooltip = document.createElement('div');
        this.tooltip.className = 'tooltip';
        document.getElementById('body').appendChild(this.tooltip);
    }
    Tooltip.prototype.getTask = function () {
        return this._task;
    };
    Tooltip.prototype.setTask = function (task) {
        this._task = assign({}, task);
    };
    Tooltip.prototype.hideTooltip = function () {
        if (this.tooltip)
            this.tooltip.style.display = 'none';
    };
    Tooltip.prototype.setTasks = function (tasks) {
        this._tasks = tasks;
    };
    Tooltip.prototype.showTaskTooltip = function (e) {
        var div = "\n            <p align=\"center\">Task</p>\n            <p align=\"left\" class=\"crop\">Task: " + this._task.title + "</p>    \n            <p >Start: " + this._task.startDate.toString().slice(0, 25) + "</p>\n            <p >Finish: " + this._task.dueDate.toString().slice(0, 25) + "</p>    \n            <p >Duration: " + this.duration() + "</p>\n            <p >% Compelete: " + this._task.complete + "</p>\n        ";
        this.tooltip.innerHTML = div;
        this.tooltip.style.display = 'block';
        this.tooltip.style.width = '250px';
        this.tooltip.style.height = '125px';
        this.setPositionToolip(this.tooltip, e);
    };
    Tooltip.prototype.showResourceTooltip = function (e) {
        var p = this._tasks.map(function (elem) {
            return "\n                <p class=\"crop\"> \n                    " + elem.startDate.toString().slice(0, 15) + " - " + elem.dueDate.toString().slice(0, 15) + " : " + elem.title + "\n                </p>\n                ";
        });
        var div = "\n            <p align=\"center\"> " + this._tasks.length + " Appointments</p>" + p.join(' ');
        this.tooltip.innerHTML = div;
        this.tooltip.style.width = '350px';
        this.tooltip.style.height = 10 + this._tasks.length * 25 + 'px';
        this.tooltip.style.display = 'block';
        this.setPositionToolip(this.tooltip, e);
    };
    Tooltip.prototype.showCompliteTooltip = function (e) {
        var div = "\n            <p align=\"center\">Task</p>\n            <p>% Compelete: " + this._task.complete + "</p>\n        ";
        this.tooltip.innerHTML = div;
        if (e) {
            this.tooltip.style.width = '130px';
            this.tooltip.style.height = '50px';
            this.tooltip.style.display = 'block';
            this.setPositionToolip(this.tooltip, e);
        }
    };
    Tooltip.prototype.showMoveTooltip = function (e) {
        var div = "\n            <p align=\"center\">Task</p>\n            <p>Task Start: " + this._task.startDate.toString().slice(0, 25) + "</p>\n            <p>Task Finish: " + this._task.dueDate.toString().slice(0, 25) + "</p>\n        ";
        this.tooltip.innerHTML = div;
        if (e) {
            this.tooltip.style.width = '280px';
            this.tooltip.style.height = '75px';
            this.tooltip.style.display = 'block';
            this.setPositionToolip(this.tooltip, e);
        }
    };
    Tooltip.prototype.showResizeTooltip = function (e) {
        var div = "\n            <p align=\"center\">Task</p>\n            <p>Duration: " + this.duration() + "</p>\n            <p>Start Time: " + this._task.startDate.toString().slice(0, 25) + "</p>\n            <p>End Time: " + this._task.dueDate.toString().slice(0, 25) + "</p>\n        ";
        this.tooltip.innerHTML = div;
        if (e) {
            this.tooltip.style.width = '280px';
            this.tooltip.style.height = '80px';
            this.tooltip.style.display = 'block';
            this.setPositionToolip(this.tooltip, e);
        }
    };
    Tooltip.prototype.duration = function () {
        var day = 86400000;
        var hour = 3600000;
        var min = 60000;
        var start = this._task.startDate.getTime();
        var end = this._task.dueDate.getTime();
        var duration = end - start;
        var fullDays = Math.floor(duration / day);
        var remainder = duration % day;
        var fullHours = Math.floor(remainder / hour);
        remainder = remainder % hour;
        var fullMin = Math.floor(remainder / min);
        remainder = remainder % min;
        var temp = '';
        if (fullDays >= 1)
            temp += fullDays + 'd ';
        if (fullHours >= 1)
            temp += fullHours + 'h ';
        if (fullMin >= 1)
            temp += fullMin + 'm';
        return temp;
    };
    Tooltip.prototype.setPositionToolip = function (element, e) {
        if (element.offsetWidth + e.clientX > window.innerWidth) {
            element.style.left = window.innerWidth - element.offsetWidth * 1.2 + 'px';
        }
        else {
            element.style.left = e.clientX - element.offsetWidth / 2 + 'px';
        }
        if (element.offsetHeight + e.clientY > window.innerHeight) {
            element.style.top = window.innerHeight - element.offsetHeight * 1.2 + 'px';
        }
        else {
            element.style.top = e.clientY + 5 + 'px';
        }
    };
    return Tooltip;
}());
/* harmony default export */ __webpack_exports__["a"] = (Tooltip);


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Code refactored from Mozilla Developer Network:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
 */



function assign(target, firstSource) {
  if (target === undefined || target === null) {
    throw new TypeError('Cannot convert first argument to object');
  }

  var to = Object(target);
  for (var i = 1; i < arguments.length; i++) {
    var nextSource = arguments[i];
    if (nextSource === undefined || nextSource === null) {
      continue;
    }

    var keysArray = Object.keys(Object(nextSource));
    for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
      var nextKey = keysArray[nextIndex];
      var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
      if (desc !== undefined && desc.enumerable) {
        to[nextKey] = nextSource[nextKey];
      }
    }
  }
  return to;
}

function polyfill() {
  if (!Object.assign) {
    Object.defineProperty(Object, 'assign', {
      enumerable: false,
      configurable: true,
      writable: true,
      value: assign
    });
  }
}

module.exports = {
  assign: assign,
  polyfill: polyfill
};


/***/ }),
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return delay; });
var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();



/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_js_datepicker__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_js_datepicker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_js_datepicker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model__ = __webpack_require__(0);


var FormField = /** @class */ (function () {
    function FormField(title, internalName, type) {
        this.title = title;
        this.internalName = internalName;
        this.type = type;
    }
    return FormField;
}());
var FORM_FIELDS = [
    new FormField('New Task', 'title', 'text'),
    new FormField('Start Date', 'startDate', 'datepicker'),
    new FormField('Due Date', 'dueDate', 'datepicker'),
    new FormField('% Complete', 'complete', 'complete'),
    new FormField('Assigned To', 'assignedTo', 'assignedTo')
];
var ModalWindow = /** @class */ (function () {
    function ModalWindow(_dataService) {
        this._dataService = _dataService;
        this.formFields = FORM_FIELDS;
        this.groupedTasks = this._dataService.getGroupedTasks();
        this.modal = document.getElementById('modalWindow');
        this.modalContent = document.getElementById('dynamicContent');
        this.closeBtn = this.createBtn('Close', 'closeModal');
        this.closeBtn.addEventListener('click', this.hide.bind(this));
    }
    ModalWindow.prototype.createBtn = function (title, id) {
        var btn = document.createElement("button");
        btn.innerHTML = title;
        btn.id = id;
        return btn;
    };
    ModalWindow.prototype.showAddNewTask = function () {
        this.modalContent.innerHTML = 'New Task <br>';
        this.generateClearFormMarkup();
        this.modal.style.display = 'block';
        var btnsContainer = document.getElementById('btnsModalContainer');
        btnsContainer.innerHTML = '';
        this.saveTaskBtn = this.createBtn('Save', 'saveTask');
        btnsContainer.appendChild(this.saveTaskBtn);
        this.saveTaskBtn.addEventListener('click', this.addNewTask.bind(this));
        btnsContainer.appendChild(this.closeBtn);
    };
    ModalWindow.prototype.showUpdateTask = function (task) {
        var _this = this;
        this.modalContent.innerHTML = 'Update Task <br>';
        this.generateUpdateTaskForm(task);
        this.modal.style.display = 'block';
        var btnsContainer = document.getElementById('btnsModalContainer');
        btnsContainer.innerHTML = '';
        this.deleteTaskBtn = this.createBtn('Delete', 'deleteTask');
        this.deleteTaskBtn.addEventListener('click', function () { return _this.deleteTask(task); });
        btnsContainer.appendChild(this.deleteTaskBtn);
        this.updateTaskBtn = this.createBtn('Update', 'updateTask');
        this.updateTaskBtn.addEventListener('click', function () { return _this.updateTask(task); });
        btnsContainer.appendChild(this.updateTaskBtn);
        btnsContainer.appendChild(this.closeBtn);
    };
    ModalWindow.prototype.show = function () {
        this.modal.style.display = 'block';
    };
    ModalWindow.prototype.hide = function () {
        this.modal.style.display = 'none';
    };
    ModalWindow.prototype.addNewTask = function () {
        var fields = this.getValueOfFileds();
        var task = this.createTaskByFromFields(fields);
        this._dataService.addTask(task);
        this.hide();
    };
    ModalWindow.prototype.deleteTask = function (task) {
        this._dataService.deleteTask(task);
        this.hide();
    };
    ModalWindow.prototype.updateTask = function (task) {
        var fields = this.getValueOfFileds();
        var newTask = this.createTaskByFromFields(fields);
        newTask.id = task.id;
        console.log('old:', task, 'new: ', newTask);
        this._dataService.updateTask(newTask);
        this.hide();
    };
    ModalWindow.prototype.generateClearFormMarkup = function () {
        for (var i = 0; i < this.formFields.length; i++) {
            switch (this.formFields[i].type) {
                case 'text':
                    this.textInput(this.formFields[i].title, this.formFields[i].internalName, true);
                    break;
                case 'datepicker':
                    this.datepickerInput(this.formFields[i]);
                    break;
                case 'complete':
                    this.completeInput(this.formFields[i]);
                    break;
                case 'assignedTo':
                    this.assignedToInput(this.formFields[i]);
                    break;
                default:
                    console.log('такого поля нет');
                    break;
            }
        }
    };
    ModalWindow.prototype.generateUpdateTaskForm = function (task) {
        for (var i = 0; i < this.formFields.length; i++) {
            switch (this.formFields[i].type) {
                case 'text':
                    this.textInput(this.formFields[i].title, this.formFields[i].internalName, true, task.title);
                    break;
                case 'datepicker':
                    this.datepickerInput(this.formFields[i], task[this.formFields[i].internalName]);
                    break;
                case 'complete':
                    this.completeInput(this.formFields[i], task.complete);
                    break;
                case 'assignedTo':
                    this.assignedToInput(this.formFields[i], task.assignedTo);
                    break;
                default:
                    console.log('такого поля нет');
                    break;
            }
        }
    };
    ModalWindow.prototype.textInput = function (title, id, required, inputValue) {
        var div = document.createElement("div");
        div.className = 'inputElement';
        var label = document.createElement("label");
        if (required) {
            title += '*';
        }
        label.innerText = title + ': ';
        label.setAttribute('for', id);
        var input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.id = id;
        input.value = inputValue ? inputValue : '';
        div.appendChild(label);
        div.appendChild(input);
        this.modalContent.appendChild(div);
    };
    ModalWindow.prototype.datepickerInput = function (formField, date) {
        if (date) {
            console.log("!!! formField.internalName: ", formField.internalName, 'date: ', date);
        }
        var div = document.createElement("div");
        div.className = 'inputElement';
        var label = document.createElement('label');
        label.innerText = formField.title + ': ';
        label.setAttribute('for', formField.internalName);
        var input = document.createElement('input');
        input.id = formField.internalName;
        div.appendChild(label);
        div.appendChild(input);
        this.modalContent.appendChild(div);
        var datepicker = __WEBPACK_IMPORTED_MODULE_0_js_datepicker__(input, {
            dateSelected: date ? date : new Date(),
        });
    };
    ModalWindow.prototype.assignedToInput = function (formField, defaultValue) {
        console.log(' defaultValue assTo', defaultValue);
        var div = document.createElement("div");
        div.className = 'inputElement';
        var label = document.createElement('label');
        label.innerText = formField.title + ': ';
        //
        var select = document.createElement('select');
        select.id = formField.internalName;
        var opt1 = document.createElement('option');
        opt1.innerText = 'null';
        opt1.value = 'null';
        select.appendChild(opt1);
        var opt2 = document.createElement('option');
        opt2.innerText = 'employee1';
        opt2.value = 'employee1';
        select.appendChild(opt2);
        var opt3 = document.createElement('option');
        opt3.innerText = 'employee2';
        opt3.value = 'employee2';
        select.appendChild(opt3);
        //
        div.appendChild(label);
        div.appendChild(select);
        this.modalContent.appendChild(div);
    };
    ModalWindow.prototype.completeInput = function (formField, defaultValue) {
        var div = document.createElement("div");
        div.className = 'inputElement';
        var label = document.createElement('label');
        label.setAttribute('for', formField.internalName);
        var input = document.createElement('input');
        input.id = formField.internalName;
        input.setAttribute('type', 'range');
        input.setAttribute('min', '0');
        input.setAttribute('max', '100');
        defaultValue ? input.setAttribute('value', "" + defaultValue) : input.setAttribute('value', '0');
        label.innerText = formField.title + ': ' + input.value;
        input.oninput = function () {
            label.innerHTML = formField.title + ': ' + input.value;
        };
        div.appendChild(label);
        div.appendChild(input);
        this.modalContent.appendChild(div);
    };
    ModalWindow.prototype.getValueOfFileds = function () {
        var fields = [];
        for (var i = 0; i < this.modalContent.childNodes.length; i++) {
            var element = this.modalContent.childNodes[i];
            if (element.tagName && element.tagName.toLowerCase() === 'div') {
                var c = this.getValueFromDiv(element);
                fields.push(c);
            }
        }
        return fields;
    };
    ModalWindow.prototype.getValueFromDiv = function (div) {
        for (var i = 0; i < div.childNodes.length; i++) {
            var element = div.childNodes[i];
            if (element.tagName && (element.tagName.toLowerCase() === 'input' || element.tagName.toLowerCase() === 'select')) {
                return {
                    key: element.id,
                    value: element.value
                };
            }
        }
    };
    ModalWindow.prototype.findValueForInternalName = function (key) {
        var elements = this.modalContent.getElementsByTagName('*');
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].id === key) {
                return elements[i].value;
            }
        }
    };
    ModalWindow.prototype.createTaskByFromFields = function (fields) {
        var task = new __WEBPACK_IMPORTED_MODULE_1__model__["d" /* Task */](null, '', null, null, new __WEBPACK_IMPORTED_MODULE_1__model__["b" /* Resource */](null, null), null);
        for (var i = 0; i < this.formFields.length; i++) {
            var value = this.findValueForInternalName(this.formFields[i].internalName);
            if (value) {
                switch (this.formFields[i].type) {
                    case 'text':
                        break;
                    case 'datepicker':
                        value = new Date(value);
                        break;
                    case 'assignedTo':
                        value = { id: value, title: value };
                }
                task[this.formFields[i].internalName] = value;
            }
        }
        var id = Math.ceil(Math.random() * 10000) + Math.ceil(Math.random() * 100) * Math.ceil(Math.random() * 1000);
        task.id = 'new' + id;
        return task;
    };
    ModalWindow.prototype.showAdvancedFilter = function () {
        this.modal.style.display = 'block';
        this.modalContent.innerHTML = ' ';
        this.modalContent.innerHTML = "\n            <div>\n                <span>Advanced Filter</span><br>\n                <input type='text'>\n                <span>Advanced Filter</span><br>\n                <input type='text'>\n                <span>Advanced Filter</span><br>\n            </div>\n        ";
        var btnsContainer = document.getElementById('btnsModalContainer');
        btnsContainer.innerHTML = '';
        btnsContainer.appendChild(this.closeBtn);
    };
    return ModalWindow;
}());
/* harmony default export */ __webpack_exports__["a"] = (ModalWindow);


/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__model__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__eventObserver__ = __webpack_require__(19);


var TASKS = [
    {
        id: "001",
        title: "task 001",
        startDate: new Date("2018-03-05T12:00:00.000Z"),
        dueDate: new Date("2018-03-30T13:38:00.000Z"),
        assignedTo: {
            id: "null",
            title: "everyone"
        },
        complete: 50
    },
    {
        id: "002",
        title: "task 002 param-pam-pam o1o2o3o4o5",
        startDate: new Date("2018-03-11T12:40:00.000Z"),
        dueDate: new Date("2018-03-16T16:00:00.000Z"),
        assignedTo: {
            id: "null",
            title: "everyone"
        },
        complete: 90
    },
    {
        id: "003",
        title: "task 003",
        startDate: new Date("2018-03-02T12:00:00.000Z"),
        dueDate: new Date("2018-03-25T12:00:00.000Z"),
        assignedTo: {
            id: "null",
            title: "everyone"
        },
        complete: 40
    },
    {
        id: "004",
        title: "task 004",
        startDate: new Date("2018-02-25T12:00:00.000Z"),
        dueDate: new Date("2018-02-25T12:00:00.000Z"),
        assignedTo: {
            id: "employee1",
            title: "everyone"
        },
        complete: 0
    },
    {
        id: "005",
        title: "task 005",
        startDate: new Date("2018-02-22T12:00:00.000Z"),
        dueDate: new Date("2018-03-27T12:00:00.000Z"),
        assignedTo: {
            id: "employee1",
            title: "everyone"
        },
        complete: 0
    },
    {
        id: "006",
        title: "task 006",
        startDate: new Date("2018-02-24T12:50:00.000Z"),
        dueDate: new Date("2018-03-24T21:00:00.000Z"),
        assignedTo: {
            id: "employee2",
            title: "everyone"
        },
        complete: 20
    },
    {
        id: "007",
        title: "about ...",
        startDate: new Date("2018-02-17T12:00:00.000Z"),
        dueDate: new Date("2018-03-30T13:38:00.000Z"),
        assignedTo: {
            id: "Bob",
            title: "everyone"
        },
        complete: 50
    },
    {
        id: "008",
        title: "make ... and ",
        startDate: new Date("2018-02-11T12:40:00.000Z"),
        dueDate: new Date("2018-03-16T16:00:00.000Z"),
        assignedTo: {
            id: "Bob",
            title: "everyone"
        },
        complete: 90
    },
    {
        id: "009",
        title: "create app",
        startDate: new Date("2018-02-20T12:00:00.000Z"),
        dueDate: new Date("2018-03-25T12:00:00.000Z"),
        assignedTo: {
            id: "Bob",
            title: "everyone"
        },
        complete: 40
    },
    {
        id: "010",
        title: "go to the //",
        startDate: new Date("2018-02-22T12:00:00.000Z"),
        dueDate: new Date("2018-02-26T12:00:00.000Z"),
        assignedTo: {
            id: "Mike",
            title: "everyone"
        },
        complete: 0
    },
    {
        id: "011",
        title: "sleep",
        startDate: new Date("2018-02-22T12:00:00.000Z"),
        dueDate: new Date("2018-02-27T12:00:00.000Z"),
        assignedTo: {
            id: "Mike",
            title: "everyone"
        },
        complete: 0
    },
    {
        id: "012",
        title: "work work work",
        startDate: new Date("2018-02-24T12:50:00.000Z"),
        dueDate: new Date("2018-02-24T21:00:00.000Z"),
        assignedTo: {
            id: "employee2",
            title: "everyone"
        },
        complete: 20
    }
];
var DataService = /** @class */ (function () {
    function DataService() {
        this.tasks = this.cloneTasks(TASKS);
        this.eventObserver = new __WEBPACK_IMPORTED_MODULE_1__eventObserver__["a" /* default */]();
    }
    DataService.prototype.getGroupedTasks = function () {
        return this.groupTasksByResources(this.tasks);
    };
    DataService.prototype.addTask = function (task) {
        this.tasks.push(task);
        this.eventObserver.broadcast(this.getGroupedTasks());
    };
    DataService.prototype.updateTask = function (task) {
        for (var i = 0; this.tasks.length; i++) {
            if (task.id === this.tasks[i].id) {
                this.tasks[i] = task;
                break;
            }
        }
        this.eventObserver.broadcast(this.getGroupedTasks());
    };
    DataService.prototype.deleteTask = function (task) {
        for (var i = 0; this.tasks.length; i++) {
            if (task.id === this.tasks[i].id) {
                this.tasks.splice(i, 1);
                break;
            }
        }
        this.eventObserver.broadcast(this.getGroupedTasks());
    };
    DataService.prototype.filterTasks = function (filterText) {
        var filteredTasks = this.tasks.filter(function (task) {
            return (task.title.toLowerCase().indexOf(filterText, 0) !== -1 ||
                task.complete.toString().toLowerCase().indexOf(filterText, 0) !== -1 ||
                task.assignedTo.id.toLowerCase().indexOf(filterText, 0) !== -1 ||
                task.startDate.toString().toLowerCase().indexOf(filterText, 0) !== -1 ||
                task.dueDate.toString().toLowerCase().indexOf(filterText, 0) !== -1);
        });
        this.eventObserver.broadcast(this.groupTasksByResources(filteredTasks));
        // return this.groupTasksByResources(filteredTasks);
    };
    //for test create many tasks
    DataService.prototype.cloneTasks = function (tasks) {
        var newArr = new Array();
        for (var i = 0; i < 1; i++) {
            for (var j = 0; j < tasks.length; j++) {
                var id = i.toString() + j.toString() + Math.ceil(Math.random() * 1000);
                var newTask = new __WEBPACK_IMPORTED_MODULE_0__model__["d" /* Task */](id, tasks[j].title, tasks[j].startDate, tasks[j].dueDate, tasks[j].assignedTo, tasks[j].complete);
                newArr.push(newTask);
            }
        }
        return newArr;
    };
    DataService.prototype.getListOfUniqResourceNames = function (tasks) {
        var uniqRes = [];
        for (var i = 0; i < tasks.length; i++) {
            if (uniqRes.indexOf(tasks[i].assignedTo.id) === -1)
                uniqRes.push(tasks[i].assignedTo.id);
        }
        return uniqRes;
    };
    DataService.prototype.groupTasksByResources = function (tasks) {
        var uniqRes = this.getListOfUniqResourceNames(tasks);
        var group = new Array(uniqRes.length);
        for (var i = 0; i < uniqRes.length; i++) {
            group[i] = new __WEBPACK_IMPORTED_MODULE_0__model__["a" /* GroupedTasks */](uniqRes[i], new Array());
            for (var j = 0; j < tasks.length; j++) {
                if (tasks[j].assignedTo.id === uniqRes[i]) {
                    group[i].tasks.push(tasks[j]);
                }
            }
        }
        return group;
    };
    return DataService;
}());
/* harmony default export */ __webpack_exports__["a"] = (DataService);


/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var EventObserver = /** @class */ (function () {
    function EventObserver() {
        this._observers = [];
    }
    // subscribe (fn:any) {
    //     this._observers.push(fn)
    // }
    // unsubscribe (fn:any) {
    //     this._observers = this._observers.filter( (subscriber:any) => subscriber !== fn)
    // }
    // broadcast (data:any) {
    //     this._observers.forEach( (subscriber:any) => subscriber(data) ) 
    // }
    EventObserver.prototype.subscribe = function (object) {
        this._observers.push(object);
    };
    EventObserver.prototype.unsubscribe = function (object) {
        this._observers = this._observers.filter(function (subscriber) { return subscriber !== object; });
    };
    EventObserver.prototype.broadcast = function (data) {
        this._observers.forEach(function (obj) { return obj.update(data); });
    };
    return EventObserver;
}());
/* harmony default export */ __webpack_exports__["a"] = (EventObserver);


/***/ })
/******/ ]);