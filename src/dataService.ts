import {Task, Resource, GroupedTasks, Scale} from './model';
import EventObserver from './eventObserver';

let TASKS: Task[] = [
    {
        id: "001", 
        title: "task 001",
        startDate: new Date("2018-03-05T12:00:00.000Z"),
        dueDate: new Date("2018-03-30T13:38:00.000Z"),
        assignedTo: {
            id: "null",
            title: "everyone"
        },
        complete: 50 
        
    },
    {
        id: "002", 
        title: "task 002 param-pam-pam o1o2o3o4o5",
        startDate: new Date("2018-03-11T12:40:00.000Z"),
        dueDate: new Date("2018-03-16T16:00:00.000Z"),
        assignedTo:  {
            id: "null",
            title: "everyone"
        },
        complete: 90 
    },
    {
        id: "003", 
        title: "task 003",
        startDate: new Date("2018-03-02T12:00:00.000Z"),
        dueDate: new Date("2018-03-25T12:00:00.000Z"),
        assignedTo: {
            id: "null",
            title: "everyone"
        },
        complete: 40 
    },
    {
        id: "004", 
        title: "task 004",
        startDate: new Date("2018-02-25T12:00:00.000Z"),
        dueDate: new Date("2018-02-25T12:00:00.000Z"),
        assignedTo: {
            id: "employee1",
            title: "everyone"
        },
        complete: 0 
    },
    {
        id: "005", 
        title: "task 005",
        startDate: new Date("2018-02-22T12:00:00.000Z"),
        dueDate: new Date("2018-03-27T12:00:00.000Z"),
        assignedTo: {
            id: "employee1",
            title: "everyone"
        },
        complete: 0 
    },
    {
        id: "006", 
        title: "task 006",
        startDate: new Date("2018-02-24T12:50:00.000Z"),
        dueDate: new Date("2018-03-24T21:00:00.000Z"),
        assignedTo: {
            id: "employee2",
            title: "everyone"
        },
        complete: 20 
    },
    {
      id: "007", 
      title: "about ...",
      startDate: new Date("2018-02-17T12:00:00.000Z"),
      dueDate: new Date("2018-03-30T13:38:00.000Z"),
      assignedTo: {
          id: "Bob",
          title: "everyone"
      },
      complete: 50 
      
  },
  {
      id: "008", 
      title: "make ... and ",
      startDate: new Date("2018-02-11T12:40:00.000Z"),
      dueDate: new Date("2018-03-16T16:00:00.000Z"),
      assignedTo:  {
          id: "Bob",
          title: "everyone"
      },
      complete: 90 
  },
  {
      id: "009", 
      title: "create app",
      startDate: new Date("2018-02-20T12:00:00.000Z"),
      dueDate: new Date("2018-03-25T12:00:00.000Z"),
      assignedTo: {
          id: "Bob",
          title: "everyone"
      },
      complete: 40 
  },
  {
      id: "010", 
      title: "go to the //",
      startDate: new Date("2018-02-22T12:00:00.000Z"),
      dueDate: new Date("2018-02-26T12:00:00.000Z"),
      assignedTo: {
          id: "Mike",
          title: "everyone"
      },
      complete: 0 
  },
  {
      id: "011", 
      title: "sleep",
      startDate: new Date("2018-02-22T12:00:00.000Z"),
      dueDate: new Date("2018-02-27T12:00:00.000Z"),
      assignedTo: {
          id: "Mike",
          title: "everyone"
      },
      complete: 0 
  },
  {
      id: "012", 
      title: "work work work",
      startDate: new Date("2018-02-24T12:50:00.000Z"),
      dueDate: new Date("2018-02-24T21:00:00.000Z"),
      assignedTo: {
          id: "employee2",
          title: "everyone"
      },
      complete: 20 
  }
];

export default class DataService {

    private tasks: Task[];
    public eventObserver: EventObserver;

    constructor() {
        this.tasks = this.cloneTasks(TASKS);
        this.eventObserver = new EventObserver();
    }

    public getGroupedTasks() {
        return this.groupTasksByResources(this.tasks);
    }

    public addTask(task:Task) {
        this.tasks.push(task);
        this.eventObserver.broadcast( this.getGroupedTasks() );
    }

    public updateTask(task:Task) {
        for (let i=0; this.tasks.length; i++) {
            if (task.id === this.tasks[i].id) {
                this.tasks[i] = task;
                break;
            }
        }
        this.eventObserver.broadcast( this.getGroupedTasks() );
    }

    public deleteTask(task:Task) {
        for (let i=0; this.tasks.length; i++) {
            if (task.id === this.tasks[i].id) {
                this.tasks.splice(i,1);
                break;
            }
        }
        this.eventObserver.broadcast( this.getGroupedTasks() );
    }

    public filterTasks(filterText:string) {
        let filteredTasks = this.tasks.filter( task => {
            return (task.title.toLowerCase().indexOf(filterText, 0) !==-1 || 
                    task.complete.toString().toLowerCase().indexOf(filterText, 0) !==-1 ||
                    task.assignedTo.id.toLowerCase().indexOf(filterText, 0) !==-1 ||
                    task.startDate.toString().toLowerCase().indexOf(filterText, 0) !==-1 ||
                    task.dueDate.toString().toLowerCase().indexOf(filterText, 0) !==-1);
        });

        this.eventObserver.broadcast ( this.groupTasksByResources(filteredTasks) )
        // return this.groupTasksByResources(filteredTasks);
    }


    //for test create many tasks
    private cloneTasks(tasks: Task[]):Task[] {
        let newArr: Task[] = new Array();
        for (let i=0; i<1; i++) {
            for (let j=0; j<tasks.length; j++) {
                let id = i.toString() + j.toString() + Math.ceil(Math.random() * 1000);
                let newTask = new Task(
                    id, 
                    tasks[j].title, 
                    tasks[j].startDate, 
                    tasks[j].dueDate, 
                    tasks[j].assignedTo, 
                    tasks[j].complete
                );
                newArr.push(newTask);
            }
        }
        return newArr;
    }

    private getListOfUniqResourceNames (tasks: Task[]): string[] {
        let uniqRes : string[] =[];
        for (let i=0; i<tasks.length; i++) {   
            if (uniqRes.indexOf(tasks[i].assignedTo.id) === -1)
                uniqRes.push(tasks[i].assignedTo.id); 
        }
        return uniqRes;
    }

    private groupTasksByResources(tasks: Task[]): GroupedTasks[] {
        let uniqRes = this.getListOfUniqResourceNames(tasks);
        let group : GroupedTasks[] = new Array(uniqRes.length);
    
        for (let i=0; i<uniqRes.length; i++) {
            group[i] = new GroupedTasks(uniqRes[i], new Array())
            for (let j=0; j<tasks.length; j++) {
                if (tasks[j].assignedTo.id === uniqRes[i]) {
                    group[i].tasks.push(tasks[j]);
                }
            }        
        }
        return group;
    }
} 