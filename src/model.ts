export class Task {
    [key: string]: any;

    id : string;
    title : string;
    startDate : Date;
    dueDate : Date;
    assignedTo : Resource;
    complete : number;

    constructor(id: string, title: string, startDate: Date, dueDate: Date, assignedTo: Resource, complete: number) {
        this.id = id;
        this.title = title;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.assignedTo = assignedTo;
        this.complete = complete;
    }
}

export class Resource {
    id: string;
    title: string;
     
    constructor(id: string, title:string) { 
        this.id = id;
        this.title = title;
    }
}

export class GroupedTasks {
    name : string;
    tasks : Task[];
    isOpen: boolean;

    constructor(name: string, tasks: Task[]) {
        this.name = name;
        this.tasks = tasks;
        this.isOpen = true;
    }
}

export enum Scale {
    Hours = "Hours",
    QDays = "QDays",
    Days = "Days",
    Weeks = "Weeks",
    ThirdsOfMonths = "ThirdsOfMonths",
    Months = "Months",
    Quarters = "Quarters",
    HalfYears = "HalfYears"
}