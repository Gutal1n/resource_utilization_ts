import {Task} from './model';
import { join } from 'path';

var assign = require('es6-object-assign').assign;

export default class Tooltip {

    private tooltip:HTMLElement;

    private _task: Task;
    private _tasks: Task[];

    constructor() {
        this.tooltip = document.createElement('div');
        this.tooltip.className = 'tooltip';
        document.getElementById('body').appendChild(this.tooltip);
    }

    public getTask():Task {
        return this._task;
    }
    public setTask(task:Task) {
        this._task = assign({}, task);
    }

    public hideTooltip() {
        if (this.tooltip)
            this.tooltip.style.display = 'none';
    }

    public setTasks(tasks:Task[]) {
        this._tasks = tasks;
    }

    public showTaskTooltip(e:Event) {
        let div = `
            <p align="center">Task</p>
            <p align="left" class="crop">Task: ${this._task.title}</p>    
            <p >Start: ${this._task.startDate.toString().slice(0,25)}</p>
            <p >Finish: ${this._task.dueDate.toString().slice(0,25)}</p>    
            <p >Duration: ${this.duration()}</p>
            <p >% Compelete: ${this._task.complete}</p>
        `;
        this.tooltip.innerHTML = div;
        this.tooltip.style.display = 'block';
        this.tooltip.style.width = '250px';
        this.tooltip.style.height = '125px';
        this.setPositionToolip(this.tooltip, e);
    }

    public showResourceTooltip(e:Event) {
        let p = this._tasks.map( (elem) => {
            return `
                <p class="crop"> 
                    ${elem.startDate.toString().slice(0,15)} - ${elem.dueDate.toString().slice(0,15)} : ${elem.title}
                </p>
                `
        });
        let div = `
            <p align="center"> ${this._tasks.length} Appointments</p>` + p.join(' ');
        this.tooltip.innerHTML = div;
        this.tooltip.style.width = '350px';
        this.tooltip.style.height =  10+ this._tasks.length * 25 + 'px';
        this.tooltip.style.display = 'block';
        this.setPositionToolip(this.tooltip, e);
    }

    public showCompliteTooltip(e?:Event) {
        let div = `
            <p align="center">Task</p>
            <p>% Compelete: ${this._task.complete}</p>
        `;
        this.tooltip.innerHTML = div;
        if (e) {
            this.tooltip.style.width = '130px';
            this.tooltip.style.height = '50px';
            this.tooltip.style.display = 'block';
            this.setPositionToolip(this.tooltip, e);
        }

    }

    public showMoveTooltip(e?:Event) {
        let div = `
            <p align="center">Task</p>
            <p>Task Start: ${this._task.startDate.toString().slice(0,25)}</p>
            <p>Task Finish: ${this._task.dueDate.toString().slice(0,25)}</p>
        `;
        this.tooltip.innerHTML =  div;
        if (e) {
            this.tooltip.style.width = '280px';
            this.tooltip.style.height = '75px';
            this.tooltip.style.display = 'block';
            this.setPositionToolip(this.tooltip, e);
        }

    }


    public showResizeTooltip(e?:Event) {
        let div = `
            <p align="center">Task</p>
            <p>Duration: ${this.duration()}</p>
            <p>Start Time: ${this._task.startDate.toString().slice(0,25)}</p>
            <p>End Time: ${this._task.dueDate.toString().slice(0,25)}</p>
        `;
        this.tooltip.innerHTML = div;
        if (e) {
            this.tooltip.style.width = '280px';
            this.tooltip.style.height = '80px';
            this.tooltip.style.display = 'block';
            this.setPositionToolip(this.tooltip, e);
        }

    }

    private duration() {
        const day = 86400000;
        const hour = 3600000; 
        const min = 60000;

        let start = this._task.startDate.getTime();
        let end = this._task.dueDate.getTime();
        let duration = end - start;

        let fullDays =Math.floor(duration / day);
        let remainder = duration % day;
        let fullHours = Math.floor(remainder / hour);
        remainder = remainder % hour;
        let fullMin = Math.floor(remainder / min);
        remainder = remainder % min;

        let temp = '';
        if (fullDays >= 1) temp += fullDays+'d ';
        if (fullHours >=1) temp += fullHours+'h ';
        if (fullMin >=1) temp+= fullMin+'m';
        return temp;
    }

    private setPositionToolip(element: HTMLElement, e:any) {
        if (element.offsetWidth + e.clientX > window.innerWidth) {
          element.style.left = window.innerWidth - element.offsetWidth * 1.2 + 'px';
        } else {
          element.style.left = e.clientX - element.offsetWidth/2 + 'px';
        }
        if (element.offsetHeight + e.clientY > window.innerHeight) {
          element.style.top = window.innerHeight - element.offsetHeight * 1.2 + 'px';
        } else {
          element.style.top = e.clientY + 5 + 'px';
        }
    }

}