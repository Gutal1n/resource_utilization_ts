import {Task, GroupedTasks, Scale} from './model';
import Settings from './settings';
import './date.extension';
import { disconnect } from 'cluster';
import { setTimeout } from 'timers';

const WEEK_CELLS = 7;
const DAY_CELLS = 24;
const QDAY_CELLS = 4;

const HEIGHT_ROW = 25;

class Matrix {
    public count: number;
    public tasks: Task[];
  
    constructor() {
      this.count = 0;
      this.tasks = new Array();
    }
}

export default class Timeline {

    parentElementHeader: HTMLElement;
    parentElementBody:HTMLElement;
    settings: Settings;

    constructor(parentElementHeader:HTMLElement, parentElementBody:HTMLElement, groupedTasks: GroupedTasks[], settings:Settings) {
        this.parentElementHeader = parentElementHeader;
        this.parentElementBody = parentElementBody;
        this.settings = settings;

        this.createTimelineHead();
        this.createTimelineBody(groupedTasks);
        this.createGrid();
    }

    update(data:GroupedTasks[]) {
        console.log('update timeline: ',data);
        this.createTimelineBody(data);
        this.createGrid();
    }

    public createGrid() {
        this.clearGrid();
        this.drawCurrentTimeLine();
        this.drawGridTimeline();

        return true;
    }

    public createTimelineHead() {
        this.parentElementHeader.style.width = this.settings.widthCanvas + 'px';
        this.parentElementHeader.innerHTML = '';
        switch (this.settings.scale) {
            case Scale.Hours:
                this.drawHeaderOfHoursScale();
                break;
            case Scale.Days:
                this.drawHeaderOfDaysScale();
                break;
            case Scale.QDays:
                this.drawHeaderOfQuaterDays();
                break;
            case Scale.Weeks:
                this.drawHeaderOfWeeks();
                break;
            case Scale.ThirdsOfMonths:
                this.drawHeaderOfThirdsOfMonths();
        }
    }
    
    public createTimelineBody(groupedTasks:GroupedTasks[]) {
        this.parentElementBody.style.width = this.settings.widthCanvas + 'px';
        this.parentElementBody.innerHTML = '';
        this.drawTasks(groupedTasks);

        return true;
    }

    public drawResource(parentRow:HTMLElement, groupedTasks:GroupedTasks) {
        let matrix = this.calculateMatrixForDrawingResource(groupedTasks);
        for(let i=0; i<matrix.length; i++) {
            if (matrix[i].count!==0) {
                let start = i;
                let width = 1;
                i++;
                while(matrix[i] && matrix[i].count===matrix[start].count) {
                  width++;
                  i++;
                }
                let div = document.createElement('div');
                div.style.width = width + 'px';
                div.style.left = start + 'px';
                div.className = 'resource';
                div.innerText = matrix[start].tasks.length === 1 ? matrix[start].tasks[0].title : matrix[start].tasks.length + " Appointments";
                this.colorIdentifierOfTask(div, matrix[start]);
                parentRow.appendChild(div);
            }
        }
    }

    private colorIdentifierOfTask(el:HTMLElement, matrix:Matrix) {
        let countTasks = matrix.count;
        if (countTasks === 1) el.classList.add('green');
        else if (countTasks === 2) el.classList.add('orange');
        else if (countTasks > 2 && countTasks <=4) el.classList.add('red');
        else if (countTasks > 4) el.classList.add('darkRed');

        for (let i=0; i<matrix.tasks.length; i++) {
            el.classList.add('id__' + matrix.tasks[i].id);
        }
    }

    private drawTasks(groupedTasks:GroupedTasks[]) {
        for(let i=0; i<groupedTasks.length; i++) {
            let row = document.createElement('div');
            row.classList.add('row');
            row.classList.add('timeline');
            row.id = 'timeline__'+groupedTasks[i].name;
            row.style.width = this.settings.widthCanvas + 'px';
            this.parentElementBody.appendChild(row);
            this.drawResource(row, groupedTasks[i]);
    
            for(let j=0; j<groupedTasks[i].tasks.length; j++) {
                let row = document.createElement('div');
                row.classList.add('row');
                row.classList.add('taskRow');
                row.id = 'timeline__' + groupedTasks[i].tasks[j].id;
                row.style.width = this.settings.widthCanvas + 'px';
                this.parentElementBody.appendChild(row);

                if (groupedTasks[i].tasks[j].startDate.getTime() !== groupedTasks[i].tasks[j].dueDate.getTime()) {
                    this.drawTask(row, groupedTasks[i].tasks[j]);
                } else {
                    this.drawMilestone(row, groupedTasks[i].tasks[j]);
                }
            }
        }
    }

    private drawMilestone(parentElement:HTMLElement, task:Task) {
        let milestone = document.createElement('div');
        milestone.className = 'milestone';
        milestone.classList.add('id__'+task.id);
        this.setLeftShift(milestone, task);
        parentElement.appendChild(milestone);
    }

    private drawTask(parentElement:HTMLElement, task:Task) {
        let taskElement = document.createElement('div'); 
        taskElement.className = 'task';
        taskElement.classList.add('id__'+task.id);
        this.setWidthTask(taskElement, task);
        this.setLeftShift(taskElement, task);
        parentElement.appendChild(taskElement);

        let taskResize = document.createElement('div');
        taskResize.className = 'taskResize';
        taskElement.appendChild(taskResize);

        let complete = document.createElement('div');
        complete.className = 'complete';
        this.setComplete(complete, taskElement, task);
        taskElement.appendChild(complete);
        
        let completeResize = document.createElement('div');
        completeResize.className = 'completeResize';
        complete.appendChild(completeResize);
    }

    private setWidthTask(element:HTMLElement, task:Task) {
        let width =  (task.dueDate.getTime() - task.startDate.getTime()) / this.settings.timePerPixel;
        element.style.width = width + 'px';
    }

    private setLeftShift(element:HTMLElement, task:Task) {
        let startPos = (task.startDate.getTime() - this.settings.startDate.getTime() ) / this.settings.timePerPixel;
        element.style.left = startPos + 'px';
    }
    
    private setComplete(completeElem:HTMLElement, taskElem:HTMLElement ,taskData:Task) {
        let width = taskData.complete * parseInt(taskElem.style.width) / 100;
        completeElem.style.width = width + 'px';
    }

    private drawHeaderOfHoursScale() {
        let dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        for(let i=0; i<dates.length; i++) {
            let day:HTMLElement = document.createElement('div');
            day.style.width = DAY_CELLS * this.settings.widthCell + 'px';
            day.style.height = this.parentElementHeader.offsetHeight + 'px';
            day.style.left = i * DAY_CELLS * this.settings.widthCell + 'px';
            day.classList.add("headerCell")
            this.parentElementHeader.appendChild(day);

            let top:HTMLElement = document.createElement('div');
            top.innerText = dates[i].toString().slice(4,15);
            top.style.height = day.offsetHeight/2 + 'px';
            top.classList.add("headerTopCell");
            day.appendChild(top);

            let bottom:HTMLElement = document.createElement('div');
            bottom.style.height = day.offsetHeight/2 + 'px';
            day.appendChild(bottom);
            for(let i=0; i<DAY_CELLS; i++) {
                let hour = document.createElement('div');
                hour.innerText = i.toString();
                hour.style.width = this.settings.widthCell + 'px';
                hour.style.height = day.offsetHeight/2 + 'px';
                hour.classList.add("headerBottomCell");
                bottom.appendChild(hour);
            }
        }
    }

    private drawHeaderOfDaysScale() {
        let dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        let arr = this.splitOnWeekArrays(dates);
        let leftShift = 0 ;
        for(let i=0; i<arr.length; i++) {
            let week:HTMLElement = document.createElement('div');
            week.style.width = arr[i].length * this.settings.widthCell + 'px';
            week.style.height = this.parentElementHeader.offsetHeight + 'px';
            week.style.left = leftShift * this.settings.widthCell + 'px';
            week.classList.add("headerCell")
            leftShift += arr[i].length;
            this.parentElementHeader.appendChild(week);

            let top:HTMLElement = document.createElement('div');
            top.innerText = (arr[i][0]).toString().slice(4,15);
            top.style.height = week.offsetHeight/2 + 'px';
            top.classList.add("headerTopCell");
            week.appendChild(top);

            let bottom:HTMLElement = document.createElement('div');
            bottom.style.height = week.offsetHeight/2 + 'px';
            week.appendChild(bottom);
            let days = this.getOnlyDayArray(arr[i]);
            for(let i=0; i<days.length; i++) {
                let day = document.createElement('div');
                day.innerText = days[i].toString();
                day.style.width = this.settings.widthCell + 'px';
                day.style.height = week.offsetHeight/2 + 'px';
                day.classList.add("headerBottomCell");
                bottom.appendChild(day);
            }
        }
    }

    private drawHeaderOfQuaterDays() {
        let dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        for(let i=0; i<dates.length; i++) {
            let day:HTMLElement = document.createElement('div');
            day.style.width = QDAY_CELLS * this.settings.widthCell + 'px';
            day.style.height = this.parentElementHeader.offsetHeight + 'px';
            day.style.left = i * QDAY_CELLS * this.settings.widthCell + 'px';
            day.classList.add("headerCell")
            this.parentElementHeader.appendChild(day);

            let top:HTMLElement = document.createElement('div');
            top.innerText = (dates[i]).toString().slice(4,15);
            top.style.height = day.offsetHeight/2 + 'px';
            top.classList.add("headerTopCell");
            day.appendChild(top);

            let bottom = document.createElement('div');
            bottom.style.height = day.offsetHeight/2 + 'px';
            day.appendChild(bottom);
            for(let i=0; i<QDAY_CELLS; i++) {
                let hour = document.createElement('div');
                hour.innerText = (i*6).toString();
                hour.style.width = this.settings.widthCell + 'px';
                hour.style.height = day.offsetHeight/2 + 'px';
                hour.classList.add("headerBottomCell");
                bottom.appendChild(hour);
            }
        }

    }

    private drawHeaderOfWeeks() {
        
    }

    private drawHeaderOfThirdsOfMonths() {
        let leftShift = 0;
        let dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        let months = this.splitOnMonthsArrays(dates);

        const WIDTH_DAY = this.parentElementHeader.offsetWidth / dates.length;
        for (let i=0; i<months.length; i++) {
            let month:HTMLElement = document.createElement('div');
            let widthMonth = WIDTH_DAY * months[i].length;
            month.style.width = widthMonth + 'px';
            month.style.height = this.parentElementHeader.offsetHeight + 'px';
            month.style.left = leftShift + 'px';
            leftShift+= widthMonth;
            month.classList.add("headerCell");
            this.parentElementHeader.appendChild(month);

            let top:HTMLElement = document.createElement('div');
            top.innerText = months[i][0].getMonthName() + ' ' + months[i][0].getFullYear();
            top.style.height = month.offsetHeight/2 + 'px';
            top.classList.add("headerTopCell");
            month.appendChild(top);

            let bottom = document.createElement('div');
            bottom.style.height = month.offsetHeight/2 + 'px';
            month.appendChild(bottom);

            let drawCell = (widthCells:number[]) => {
                for(let j=0; j<widthCells.length; j++) {
                    if (widthCells[j] ===0) continue;
                    let cell = document.createElement('div');
                    cell.style.width = widthCells[j] + 'px';
                    cell.style.height = month.offsetHeight/2 + 'px';
                    if (widthCells[j]>0) {
                        cell.innerText = (j===0 || j === 1) ? ((j===0) ? "S" : "M") : "E";
                    }
                    cell.classList.add("headerBottomCell");
                    bottom.appendChild(cell);
                }
            }

            if (i!==0 && i!==months.length-1) {
                let w = [widthMonth/3, widthMonth/3,widthMonth/3];
                drawCell(w);
            } else if (i===0) {
                let w3 = (widthMonth - WIDTH_DAY*10 > 0) ? WIDTH_DAY*10 : widthMonth;
                let w2 = (widthMonth - w3 - WIDTH_DAY*10 > 0)? WIDTH_DAY*10 : widthMonth - w3;
                let w1 = widthMonth - w3 - w2;
                let w = [w1, w2, w3];
                drawCell(w);
            } else if (i===months.length-1) {
                let w1 = (widthMonth - WIDTH_DAY*10 > 0) ? WIDTH_DAY*10 : widthMonth;
                let w2 = (widthMonth - w1 - WIDTH_DAY*10 > 0)? WIDTH_DAY*10 : widthMonth - w1;
                let w3 = widthMonth - w1 - w2;
                let w = [w1, w2, w3];
                drawCell(w);
            }

        }
    }

    private splitOnWeekArrays(dates: Date[]) {
        let arr = new Array();
        let weekArr = new Array();
        while(dates.length>0) {
            weekArr.push(dates[0]);
            if (dates[0].getDay() ==6) {
                arr.push(weekArr);
                weekArr = new Array();
            }
            dates.shift();
        }
        if (weekArr.length>0) arr.push(weekArr);
        return arr;
    }

    private splitOnMonthsArrays(dates: Date[]) {
        let arr = new Array();
        let monthArr = new Array();
        monthArr.push(dates[0]);
        for (let i=1; i<dates.length; i++) {
            let str = dates[i].getMonthName()+dates[i].getFullYear();
            let previous = dates[i-1].getMonthName()+dates[i-1].getFullYear();
            if (str === previous) {
                monthArr.push(dates[i])
            } else {
                arr.push(monthArr);
                monthArr = new Array();
                monthArr.push(dates[i]);
            }
            if (i===dates.length-1 && monthArr.length>0) {
                arr.push(monthArr);
            } 
        }
        return arr;
    }

    private getOnlyDayArray(arr: Date[]): number[] {   
        let dateArray: number[] = new Array();
        for (let i=0; i<arr.length; i++) {
            dateArray[i] = arr[i].getDate();
        }
        return dateArray;
    }

    private calculateMatrixForDrawingResource(groupedTasksByResource:GroupedTasks){
        let matrix: Matrix[] = new Array(this.settings.widthCanvas);
        for (let i=0; i<matrix.length; i++) {
            matrix[i] = new Matrix();
        }

        for(let i=0; i<groupedTasksByResource.tasks.length; i++) {
          if (this.checkOverlapDatesWithScale(groupedTasksByResource.tasks[i].startDate, 
                                              groupedTasksByResource.tasks[i].dueDate)) {
            let startPos = Math.floor(
              (groupedTasksByResource.tasks[i].startDate.getTime() - this.settings.startDate.getTime() ) / this.settings.timePerPixel
            );
            let widthRect = Math.ceil(
              (groupedTasksByResource.tasks[i].dueDate.getTime() - groupedTasksByResource.tasks[i].startDate.getTime()) / this.settings.timePerPixel
            );
    
            if (startPos<0) {
              widthRect = startPos + widthRect;
              startPos=0; 
            }
            if (startPos+widthRect > this.settings.widthCanvas) {
              widthRect = this.settings.widthCanvas - startPos ;
            }
            for(let j=startPos; j<startPos+widthRect; j++) {
              matrix[j].count++;
              matrix[j].tasks.push(groupedTasksByResource.tasks[i]);
            }
          }
        }
        return matrix;
    }

    private checkOverlapDatesWithScale(start:Date, end:Date):boolean {
        return this.settings.startDate.getTime() < end.getTime() &&
                start.getTime() < this.settings.endDate.getTime();
    }

    private checkOverlapDateWithScale(date:Date):boolean {
        return this.settings.startDate.getTime() < date.getTime() && 
                date.getTime() < this.settings.endDate.getTime();
    }

    private drawCurrentTimeLine() {
        let today = new Date();
        if (!this.checkOverlapDateWithScale(today)) return;
        today.setHours(0, 0, 0, 0);
    
        let x = (today.getTime() - this.settings.startDate.getTime()) / this.settings.timePerPixel;
        let line = document.createElement('div');
        line.style.height = this.calcCountRow() * HEIGHT_ROW + 'px';
        line.style.left = x + 'px';
        line.className = 'currentTimeline';
        this.parentElementBody.appendChild(line);
    }

    private drawGridTimeline() {
        switch (this.settings.scale) {
            case Scale.Hours:
                this.gridOfHoursScale();
                break;
            case Scale.Days:
                this.gridOfDaysScale();
                break;
            case Scale.QDays:
                this.gridOfQDaysScale();
                break;
            case Scale.Weeks:
                this.gridOfWeeksScale();
                break;
            case Scale.ThirdsOfMonths:
                this.gridOfThirdsOfMonths();
                break;
        }
    }

    private gridOfQDaysScale() {
        let height = this.calcCountRow() * HEIGHT_ROW;
        for(let i=0; i<this.parentElementBody.offsetWidth; i+= QDAY_CELLS * this.settings.widthCell) {
            let line = document.createElement('div');
            line.style.height = height + 'px';
            line.style.left = i + 'px';
            line.className = 'gridLine';
            this.parentElementBody.appendChild(line);
        }
    }

    private gridOfWeeksScale() {

    }

    private gridOfHoursScale() {
        let height = this.calcCountRow() * HEIGHT_ROW;
        for(let i=0; i<this.parentElementBody.offsetWidth; i+= DAY_CELLS * this.settings.widthCell) {
            let line = document.createElement('div');
            line.style.height = height + 'px';
            line.style.left = i + 'px';
            line.className = 'gridLine';
            this.parentElementBody.appendChild(line);
        }
    }

    private gridOfDaysScale() {
        let height = this.calcCountRow() * HEIGHT_ROW;
        let leftShift = 0;
        let dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        let arr = this.splitOnWeekArrays(dates);
        for(let i=0; i<arr.length; i++) {
            let line = document.createElement('div');
            line.style.height = height + 'px';
            line.style.left = leftShift + 'px';
            line.className = 'gridLine';
            this.parentElementBody.appendChild(line);

            leftShift += this.settings.widthCell * arr[i].length;
        }
    }

    private gridOfThirdsOfMonths() {
        let height = this.calcCountRow() * HEIGHT_ROW;
        let dates = Date.prototype.getArrayDates(this.settings.startDate, this.settings.endDate);
        let months = this.splitOnMonthsArrays(dates);

        const WIDTH_DAY = this.parentElementHeader.offsetWidth / dates.length;
        let leftShift = 0;
        for (let i=0; i<months.length; i++) {
            let line = document.createElement('div');
            line.style.height = height + 'px';
            line.style.left = leftShift + 'px';
            line.className = 'gridLine';
            this.parentElementBody.appendChild(line);

            leftShift += WIDTH_DAY * months[i].length;
        }
    }

    private calcCountRow() {
        let count = 0; 
        let elements = this.parentElementBody.childNodes;
        for(let i=0 ; i<elements.length; i++) {
            if ( (elements[i] as HTMLElement).classList.contains('row') && (elements[i] as HTMLElement).style.display !== 'none') {
                count++;
            }
        }
        return count;
    }

    private clearGrid() {
        let grid = document.getElementsByClassName('gridLine');
        let currentLine = document.getElementsByClassName('currentTimeline')[0];
        if (currentLine) {
            let parentCurLine: HTMLElement = currentLine.parentElement;
            parentCurLine.removeChild(currentLine);
        }
        for(let i=grid.length; i>0; i--) {
            let parentGrid: HTMLElement = grid[0].parentElement;
            parentGrid.removeChild(grid[0]);
        }
        
    }
}