import {GroupedTasks, Task} from './model';

const COLUMN_NAMES:string[] = [
    "Id",
    "Title",
    "Start Date",
    "Due Date",
    "Complete"
];

const COLUMN_LENGHT:number = 6;
const COLUMN_WIDTH: number = 150;

export default class Table {

    tableHead:HTMLElement;
    tableBody:HTMLElement;

    columnNames: string[];

    constructor(groupedTasks: GroupedTasks[]) {
        this.tableHead = document.getElementById('tableHead');
        this.tableBody = document.getElementById('scrollTableBody');

        this.fillColumnNames(COLUMN_NAMES);

        this.setWidthContainers(this.columnNames.length * 150);

        this.createTableHead();
        this.createTableBody(groupedTasks);
    }

    update(data:GroupedTasks[]) {
        console.log('update table: ',data);
        this.createTableBody(data);
    }

    setWidthContainers(width:number) {
        if (width < this.columnNames.length * COLUMN_WIDTH) {
            width = this.columnNames.length * COLUMN_WIDTH;
        }
        document.getElementById('tableHead').style.width = width + 'px';
        document.getElementById('tableBody').style.width = width + 'px';
    }

    fillColumnNames(columnNames: string[]) {
        this.columnNames = columnNames;
        this.columnNames.push(' ');
    }

    // createTableHead(groupedTasks: GroupedTasks[]) {
    createTableHead() {
        let table = document.createElement('table');
        table.className = 'tableHead';
    
        let thead = document.createElement("thead");
        table.appendChild(thead);
    
        let tr = document.createElement('tr');
     
        thead.appendChild(tr);
        
        for(let i=0; i<this.columnNames.length; i++) {
            let th = document.createElement('th');
            th.innerText = this.columnNames[i];
            th.className = "th__" + i;
            if (i === this.columnNames.length-1) {
                th.classList.add('last');
            }
            tr.appendChild(th);
        }
        // let obj = groupedTasks[0].tasks[0];
        // for (let key in obj) {
        //     if (key==="assignedTo") continue;
        //     let th = document.createElement('th');
        //     key = (key.charAt(0).toUpperCase() + key.slice(1)).split(/(?=[A-Z])/).join(' ');
        //     th.innerText = key;
        //     tr.appendChild(th);
        // }
        this.tableHead.appendChild(table);
    }

    createTableBody(groupedTasks: GroupedTasks[]) {
        this.tableBody.innerHTML= '';
    
        let tbody = document.createElement('tbody');
    
        for(let i=0; i<groupedTasks.length; i++) {
            let resouseTr = document.createElement("tr");
            resouseTr.className = 'resourceTd'
            resouseTr.id ='tableTr__' + groupedTasks[i].name;
            let resTd = document.createElement('td');
            resTd.colSpan = COLUMN_LENGHT;
            resTd.innerText = `(${groupedTasks[i].tasks.length}) ${groupedTasks[i].name}`;
            resouseTr.appendChild(resTd);
            tbody.appendChild(resouseTr);
    
            for(let j=0; j<groupedTasks[i].tasks.length; j++) {
                let taskTr = document.createElement('tr');
                taskTr.id ='tableTr__' + groupedTasks[i].tasks[j].id;

                let idTd = document.createElement('td');
                idTd.innerText = groupedTasks[i].tasks[j].id;
                idTd.id = 'id__' + groupedTasks[i].tasks[j].id;
    
                let titleTd = document.createElement('td');
                titleTd.innerText = groupedTasks[i].tasks[j].title;
                titleTd.id = 'title__' + groupedTasks[i].tasks[j].id;
    
                let startTd = document.createElement('td');
                startTd.innerText = groupedTasks[i].tasks[j].startDate.toString().slice(0,25);
                startTd.id = 'startDate__' + groupedTasks[i].tasks[j].id;
    
                let dueTd = document.createElement('td');
                dueTd.innerText = groupedTasks[i].tasks[j].dueDate.toString().slice(0,25);
                dueTd.id = 'dueDate__' + groupedTasks[i].tasks[j].id;
    
                let completeTd = document.createElement('td');
                completeTd.innerText = groupedTasks[i].tasks[j].complete.toString() + ' %';
                completeTd.id = 'complete__' + groupedTasks[i].tasks[j].id;

                let empty = document.createElement('td');
                empty.classList.add('last');
    
                taskTr.appendChild(idTd);
                taskTr.appendChild(titleTd);
                taskTr.appendChild(startTd);
                taskTr.appendChild(dueTd);
                taskTr.appendChild(completeTd);
                taskTr.appendChild(empty);
    
                tbody.appendChild(taskTr);

                if (!groupedTasks[i].isOpen) {
                    taskTr.style.display = 'none'
                };
            }
        }
        this.tableBody.appendChild(tbody);

        return true;
    }

    changeDataInTr(idTr:string, task:Task) {
        let elements = document.getElementById('tableTr__'+idTr).childNodes;

        for(let i=0; i<elements.length; i++) {
            let id = (elements[i] as HTMLElement).id;
            let field = id.slice(0, id.indexOf('_'));
            
            for (let obj in task) {
                if (field === obj) {
                    if (task[obj] instanceof Date) {
                        (elements[i] as HTMLElement).innerHTML = task[obj].toString().slice(0,25);
                    } else {
                        (elements[i] as HTMLElement).innerHTML = task[obj];
                    }
                } 
            }
        }
    }
    
}
