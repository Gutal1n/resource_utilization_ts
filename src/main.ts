// import * as Split from '../node_modules/split.js/split';
// import Split from 'split.js';
const Split = require( 'split.js' );

import syncScroll from './syncScroll';
import {delay} from './delay';
import {Task, Resource, GroupedTasks, Scale} from './model';
import Settings from './settings';

import * as Datepicker from 'js-datepicker';  /* https://github.com/qodesmith/datepicker */
import Table from './table';
import Timeline from './timeline';
import Tooltip from './tooltip';
import ModalWindow from './modalWindow';
import DataService from './dataService';

export default class Main {
    dataService: DataService;
    groupedTasks: GroupedTasks[];


    settings:Settings;
    tooltip:Tooltip;
    table:Table;
    timeline:Timeline;


    targetDragAndResize:HTMLElement;
    
    btnAddNewTask:HTMLElement;
    btnAdvancedFilter:HTMLElement;

    btnScale:HTMLElement;
    btnScaleHours:HTMLElement;
    btnScaleDays:HTMLElement;
    btnScaleQuarterDays: HTMLElement;
    btnScaleWeeks: HTMLElement;
    btnScale3Months: HTMLElement;
    btnScaleMonths: HTMLElement;
    btnScaleQuarters: HTMLElement;
    btnScaleHalfYears: HTMLElement;

    timelineHead:HTMLElement;
    timelineBody:HTMLElement;

    btnBack:HTMLElement;
    btnForward:HTMLElement;

    datepicker:any;

    currentHoverTarget: HTMLElement;

    inputFilter: HTMLElement;
    filterText:string;


    modalWindow: ModalWindow;

    update(data:GroupedTasks[]) {
        this.groupedTasks = data;
    } 

    constructor() {

        this.dataService = new DataService();
        this.groupedTasks = this.dataService.getGroupedTasks();
        this.dataService.eventObserver.subscribe(this);

        window.addEventListener('load', () => {
            syncScroll("tableBody", "timelineBody");
        
            this.changeScale();
        });
        
        let leftDiv = document.getElementById("left");
        let rightDiv = document.getElementById("right");
        Split([leftDiv, rightDiv], {
            sizes: [35, 65],
            minSize: 300,
            onDrag: ()=>{
                let left = document.getElementById('left').offsetWidth;
                this.table.setWidthContainers(left);
            }
        });

        
        this.settings = new Settings();
        this.tooltip = new Tooltip();

        
        this.showDates();

        this.table = new Table(this.groupedTasks);
        this.dataService.eventObserver.subscribe(this.table);


        this.timelineHead = document.getElementById('timelineHeader');
        this.timelineBody = document.getElementById('timelineBody');

        this.timeline = new Timeline(this.timelineHead, this.timelineBody, this.groupedTasks, this.settings);
        this.dataService.eventObserver.subscribe(this.timeline);

        
        this.targetDragAndResize = null;
        
        this.timelineBody.addEventListener("mousedown", this.dragAndResize.bind(this));
        this.timelineBody.addEventListener("mousemove", this.toolTip.bind(this));
        this.timelineBody.addEventListener("mouseleave", () => {
            this.tooltip.hideTooltip();
        });


        this.modalWindow = new ModalWindow(this.dataService);

        this.btnAddNewTask = document.getElementById('btnAddNewTask');
        this.btnAddNewTask.addEventListener('click', this.modalWindow.showAddNewTask.bind(this.modalWindow));
        this.btnAdvancedFilter = document.getElementById('advancedFilter');
        this.btnAdvancedFilter.addEventListener('click', this.modalWindow.showAdvancedFilter.bind(this.modalWindow));


        window.ondblclick = (e:any) => {
            if (e.target.classList.contains('taskRow')) {
                let taskId = e.target.id.slice(10);
                let task = this.findTaskByID(taskId);
                console.log(task);
                this.modalWindow.showUpdateTask(task);
            }
        }

        this.btnScale = document.getElementById('btnScale');
        this.btnScaleHours= document.getElementById('scaleHours');
        this.btnScaleDays= document.getElementById('scaleDays');
        this.btnScaleQuarterDays = document.getElementById('scaleQDays');
        this.btnScaleWeeks = document.getElementById('scaleWeeks');
        this.btnScale3Months = document.getElementById('scale3Months');
        this.btnScaleMonths= document.getElementById('scaleMonths');
        this.btnScaleQuarters= document.getElementById('scaleQuarters');
        this.btnScaleHalfYears= document.getElementById('scaleHalfYears');
        
        this.btnScaleHours.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleDays.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleQuarterDays.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleWeeks.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScale3Months.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleMonths.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleQuarters.addEventListener('click', this.handlerChangeScale.bind(this));
        this.btnScaleHalfYears.addEventListener('click', this.handlerChangeScale.bind(this));


        this.btnBack = document.getElementById('btnBack');
        this.btnForward = document.getElementById('btnForward');
        
        this.btnBack.addEventListener('click', ()=>{
            this.timeForward(false);
            this.showDates();
            this.timeline.createTimelineHead();
            this.timeline.createTimelineBody(this.groupedTasks);
            this.timeline.createGrid();
        });

        this.btnForward.addEventListener('click', ()=>{
            this.timeForward(true);
            this.showDates();
            this.timeline.createTimelineHead();
            this.timeline.createTimelineBody(this.groupedTasks);
            this.timeline.createGrid();
        });

        let datepickerForm = document.getElementById('datepickerForm');
        this.datepicker = Datepicker(datepickerForm, {
            dateSelected: this.settings.selectedDate,
            onSelect: (instance:any) => {
                this.settings.selectedDate = instance.dateSelected;
                this.showDates();
                this.timeline.createTimelineHead();
                this.timeline.createTimelineBody(this.groupedTasks);
                this.timeline.createGrid();
            }
        });

        this.table.tableBody.onmousedown = (e) => {
            let target = (e.target as HTMLElement).parentElement;
            if (target.classList.contains('resourceTd')) {
                let resourceName =  target.id.slice(9);
                let resource = this.findResourceByName(resourceName);
                resource.isOpen = !resource.isOpen;
                this.hideOrExpandTasks(resource);
                this.timeline.createGrid();
            }
        }

        this.timeline.parentElementBody.onmousedown = (e) => {
            let setRigthTarget = (target:HTMLElement) => {
                let end:boolean = false;
                let newTarget:HTMLElement = null;
                while(!end) {
                    if (target.classList.contains('row')) {
                        newTarget = target;
                        end = true;
                    } else if (target.classList.contains('timelineBody')) {
                        end = true;
                    }
                    target = target.parentElement;
                }
                return newTarget;
            }

            let target = setRigthTarget(e.target as HTMLElement);
            if (target !==null && target.classList.contains('timeline')) {
                let id =target.id.slice(10);
                let resource = this.findResourceByName(id);
                resource.isOpen = !resource.isOpen;
                this.hideOrExpandTasks(resource);
                this.timeline.createGrid();
            }
        }

        // string highlighting
        this.currentHoverTarget = null;
        this.table.tableBody.onmouseover = (e) => {
            let target  = e.target as HTMLElement;
            if (this.currentHoverTarget !==null) {
                this.currentHoverTarget.classList.remove('rowHover');
            }
            if (target.tagName.toLowerCase() === 'td') {
                this.currentHoverTarget = document.getElementById('timeline__'+ target.parentElement.id.slice(9));
                this.currentHoverTarget.classList.add('rowHover');
            }
        }
        this.table.tableBody.onmouseleave = (e) => {
            if (this.currentHoverTarget !== null) {
                this.currentHoverTarget.classList.remove('rowHover');
                this.currentHoverTarget =  null;
            }
        }
        this.timelineBody.onmouseover = (e) => {
            let target  = e.target as HTMLElement;
            if (this.currentHoverTarget !==null) {
                this.currentHoverTarget.classList.remove('rowHover');
            }
            if (target && target.classList.contains('row')) {
                this.currentHoverTarget = document.getElementById('tableTr__'+ target.id.slice(10));
                this.currentHoverTarget.classList.add('rowHover');
            }
        }
        this.timelineBody.onmouseleave = (e) => {
            if (this.currentHoverTarget !== null) {
                this.currentHoverTarget.classList.remove('rowHover');
                this.currentHoverTarget =  null;
            }
        }


        this.inputFilter = document.getElementById('inputFilter');
        this.inputFilter.onkeyup =  () => {
            delay( ()=> {
                this.filterText = (this.inputFilter as HTMLInputElement).value.toLowerCase();
                

                this.dataService.filterTasks(this.filterText);
                // this.groupedTasks = this.dataService.filterTasks(this.filterText);

                // this.table.createTableBody(this.groupedTasks);
                // this.timeline.createTimelineBody(this.groupedTasks);
                // this.timeline.createGrid();
            }, 500)
        }
    }



    private showDates() {
        let stDate: HTMLElement = document.getElementById('startDate_datepicker');
        let endDate: HTMLElement = document.getElementById('endDate_datepicker');
        stDate.innerText = this.settings.startDate.toString().slice(0,15);
        endDate.innerText = this.settings.endDate.toString().slice(0,15);
    }


    private dragAndResize(e:any) {
        e.stopPropagation();

        if (e.target.classList.contains('task')) {
            this.targetDragAndResize = e.target;
            this.drag(e);
        } else if (e.target.classList.contains('complete')) {
            this.targetDragAndResize = e.target.parentNode;
            this.drag(e);
        } else if (e.target.classList.contains('milestone')) {
            this.targetDragAndResize = e.target;
            this.drag(e);
        } else if (e.target.classList.contains('completeResize')) {
            this.targetDragAndResize = e.target.parentNode;
            this.resizeComplete(e);
        } else if (e.target.classList.contains('taskResize')) {
            this.targetDragAndResize = e.target.parentNode;
            this.resizeTask(e);
        }
    }
    
    private drag(e:any) {
        this.targetDragAndResize.ondragstart = function () { return false; }

        this.tooltip.showMoveTooltip(e);
    
        let startPos = parseFloat(this.targetDragAndResize.style.left);
        let startMouseX = e.clientX;
        let dx = 0;

        let id = this.getListOfId(this.targetDragAndResize.classList)[0];
        let task:Task = this.findTaskByID(id);

        let previousPos = e.clientX;
        
        let move = (e:any) => {
            dx = e.clientX - startMouseX;
            this.targetDragAndResize.style.left = startPos + dx + 'px';

            this.shiftOfDateInTask(this.tooltip.getTask(), e.clientX - previousPos);
            this.tooltip.showMoveTooltip();
            previousPos = e.clientX;
        }
    
        document.onmousemove = (e) => {
            move(e);           
        }
    
        document.onmouseup = (e) => {
            this.shiftOfDateInTask(task, dx);
            this.table.changeDataInTr(id, task);
            this.redrawResource(task);
            this.targetDragAndResize = null;
            document.onmousemove = null;
            document.onmouseup = null;
        }
    }

    private resizeTask(e:any) {
        this.targetDragAndResize.ondragstart = function () { return false; }

        let id = this.getListOfId(this.targetDragAndResize.classList)[0];
        let task = this.findTaskByID(id);
        this.tooltip.setTask(task);
        this.tooltip.showResizeTooltip(e);

        let startWidth = parseFloat(this.targetDragAndResize.style.width);
        let startMouseX = e.clientX;
        let dx = 0;

        let previousPos = e.clientX;

        let changeWidthCompleteDiv = (task:Task) => {
            let childs = this.targetDragAndResize.childNodes;
            let complete:HTMLElement = null;
            for (let i=0; i<childs.length; i++) {
                if ((childs[i] as HTMLElement).classList.contains('complete')) {
                    complete = childs[i] as HTMLElement;
                    break;
                }
            }
            if (complete) {
                complete.style.width = this.targetDragAndResize.offsetWidth * task.complete / 100 + 'px';
            }
        }

        let move = (e:any) => {
            dx = e.clientX - startMouseX;

            if (startWidth + dx < 5) {
                dx = - (startWidth - 5)
            }

            this.targetDragAndResize.style.width = startWidth + dx + 'px';

            if (startWidth + dx > 5) {
                this.shiftDueDateInTask(this.tooltip.getTask(), e.clientX - previousPos);
            }
            this.tooltip.showResizeTooltip();
            previousPos = e.clientX;
            changeWidthCompleteDiv(task);
        }

        document.onmousemove = (e) => {
            move(e);
        }

        document.onmouseup = (e) => {
            this.shiftDueDateInTask(task, dx);
            this.table.changeDataInTr(id, task);
            this.redrawResource(task);
            this.targetDragAndResize = null;
            document.onmousemove = null;
            document.onmouseup = null;
        }
    }
    
    private resizeComplete(e:any) {
        this.targetDragAndResize.ondragstart = function () { return false; }

        let id = this.getListOfId(this.targetDragAndResize.parentElement.classList)[0];
        let task = this.findTaskByID(id);
        this.tooltip.setTask(task);
        this.tooltip.showCompliteTooltip(e);

        let startWidth = parseFloat(this.targetDragAndResize.style.width);
        let taskWidth = parseFloat(this.targetDragAndResize.parentElement.style.width);
        let startMouseX = e.clientX;
        let dx = 0;

        let complete = task.complete;

        let move = (e:any) => {
            dx = e.clientX - startMouseX;

            let newWidth:number;
            if (startWidth + dx < 0) {
                newWidth = 0;
            } else if (startWidth + dx > taskWidth) {
                newWidth = taskWidth;
            } else {
                newWidth = startWidth + dx
            }

            this.targetDragAndResize.style.width = newWidth + 'px';
            complete  = this.calcComplete(newWidth, taskWidth);
            this.tooltip.getTask().complete = complete;
            this.tooltip.showCompliteTooltip();
        }

        document.onmousemove = (e) => {
            move(e);
        }

        document.onmouseup = (e) => {
            this.changeCompleteInTask(task, complete);
            this.table.changeDataInTr(id, task);
            this.targetDragAndResize = null;
            document.onmousemove = null;
            document.onmouseup = null;
        }
    }

    private toolTip(e:any) {
        if (this.targetDragAndResize !== null) return;

        let target: HTMLElement = e.target;
        let listOfId:string[] = new Array();
        if (target.classList.contains('task') || target.classList.contains('milestone')) {
            listOfId = this.getListOfId(target.classList);
        } else if (target.classList.contains('complete')) {
            listOfId = this.getListOfId( (target.parentNode as HTMLElement).classList);
        }  else if (target.classList.contains('resource')) {
            listOfId = this.getListOfId(target.classList);
        } else {
            this.tooltip && this.tooltip.hideTooltip();
            return;
        }
        
        let task:Task;
        if (listOfId.length === 1) {
            let task = this.findTaskByID(listOfId[0]);
            this.tooltip.setTask(task);
            this.tooltip.showTaskTooltip(e);
        } else if (listOfId.length > 1) {
            let tasks:Task[] = new Array();
            listOfId.forEach( (element) => {
                tasks.push(this.findTaskByID(element));
            });
            this.tooltip.setTasks(tasks);
            this.tooltip.showResourceTooltip(e);
        }
    }

    private getListOfId(classList: DOMTokenList) {
        let listOfId:string[] = new Array() ;
        for(let i=0; i<classList.length; i++) {
            if ( classList[i].indexOf('id__') !== -1) 
            listOfId.push(classList[i].toString().slice(4));
        }
        return listOfId;
    }
    
    private findTaskByID(id:string):Task {
        let task:Task;
        for(let i=0; i<this.groupedTasks.length; i++) {
            for(let j=0; j<this.groupedTasks[i].tasks.length; j++) {
                if (this.groupedTasks[i].tasks[j].id === id) {
                    task = this.groupedTasks[i].tasks[j];
                    return task;
                } 
            }
        }
        return task;
    }
    
    private changeScale() {
        this.btnScale.innerText = "Scale: " + this.settings.scale;
    }
    
    private handlerChangeScale(e:Event) {
        this.settings.scale = <Scale>e.srcElement.getAttribute('data-scale');
        this.changeScale();
        this.showDates();
        this.timeline = new Timeline(this.timelineHead, this.timelineBody, this.groupedTasks, this.settings);
        console.log(this.settings)
    }
    
    private timeForward(isForward: boolean) {
        let direction : number = (isForward) ? 1 : -1;
        switch (this.settings.scale) {
            case Scale.Hours:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000*60*60*24 * 3);
                break;
            case Scale.Days:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000*60*60*24*80);
                break;
            case Scale.QDays:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000*60*60*24*8);
                break;
            case Scale.Weeks:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000*60*60*24*112);
                break;
            case Scale.ThirdsOfMonths:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000*60*60*24*200);
                break;
            case Scale.Months:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000*60*60*24*650);
                break;
            case Scale.Quarters:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000*60*60*24*2000);
                break;
            case Scale.HalfYears:
                this.settings.selectedDate = new Date(this.settings.selectedDate.getTime() + direction * 1000*60*60*24*7300);
                break;
        }
        this.datepicker.dateSelected = this.settings.selectedDate;
        this.datepicker.setDate(new Date(this.settings.selectedDate.toString()));
    }

    private shiftOfDateInTask(task:Task, pixelShift:number) {
        let dateShift = pixelShift * this.settings.timePerPixel;
        task.startDate = new Date(task.startDate.getTime() + dateShift);
        task.dueDate = new Date(task.dueDate.getTime() + dateShift);
        return task;
    }

    private shiftDueDateInTask(task:Task, pixelShift:number) {
        let dateShift = pixelShift * this.settings.timePerPixel;
        task.dueDate = new Date(task.dueDate.getTime() + dateShift);
        return task;
    }

    private changeCompleteInTask(task:Task, newComplete:number) {
        task.complete = newComplete;
    }

    private calcComplete(widthComplete:number, widthTask:number):number {
        let res = 100 * widthComplete / widthTask
        if (res<0.4) return 0;
        return Math.ceil(res);
    }


    private findAllTasksOfResousceByTask(task:Task) {
        for(let i=0; i<this.groupedTasks.length; i++) {
            if (this.groupedTasks[i].name === task.assignedTo.id) {
                return this.groupedTasks[i];
            }
        }
    }

    private findRowOfResource(nameRes:string) {
        return document.getElementById('timeline__' + nameRes);
    }
    
    private redrawResource(task: Task) {
        let res = this.findAllTasksOfResousceByTask (task);
        let row = this.findRowOfResource(res.name);
        row.innerHTML = '';
        this.timeline.drawResource(row, res);
    }

    private findResourceByName(name:string) {
        for(let i=0; i<this.groupedTasks.length; i++) {
            if (name === this.groupedTasks[i].name)
            return this.groupedTasks[i];
        }
    }

    private hideOrExpandTasks(resource:GroupedTasks) {
        for(let i=0; i<resource.tasks.length; i++) {
            let idTr = "tableTr__" + resource.tasks[i].id;
            let idTimeline = "timeline__" + resource.tasks[i].id;
            let element = document.getElementById(idTr);
            let elementTimeline = document.getElementById(idTimeline);
            if (resource.isOpen) {
                element.style.display = 'table-row';
                elementTimeline.style.display = 'block';
            } else {
                element.style.display = 'none';
                elementTimeline.style.display = 'none';
            }
        }
    }
}

