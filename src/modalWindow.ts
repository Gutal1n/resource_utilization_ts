import * as Datepicker from 'js-datepicker';
import {Task, GroupedTasks, Resource} from './model';
import DataService from './dataService';

class FormField {
    title:string;
    internalName: string;
    type: string;

    constructor(title:string, internalName:string, type:string) {
        this.title = title;
        this.internalName = internalName;
        this.type = type;
    }
}

const FORM_FIELDS: FormField[] = [
    new FormField('New Task', 'title', 'text'),
    new FormField('Start Date', 'startDate', 'datepicker'),
    new FormField('Due Date', 'dueDate', 'datepicker'),
    new FormField('% Complete', 'complete', 'complete'),
    new FormField('Assigned To', 'assignedTo', 'assignedTo')
];

export default class ModalWindow {

    modal: HTMLElement;

    modalContent: HTMLElement;

    saveTaskBtn:HTMLElement;
    closeBtn: HTMLElement;
    deleteTaskBtn:HTMLElement;
    updateTaskBtn:HTMLElement;

    formFields: FormField[];

    groupedTasks: GroupedTasks[];
    
    constructor(private _dataService: DataService) {
        this.formFields = FORM_FIELDS;

        this.groupedTasks = this._dataService.getGroupedTasks();
        
        this.modal = document.getElementById('modalWindow');
        this.modalContent = document.getElementById('dynamicContent');

        
        this.closeBtn = this.createBtn('Close', 'closeModal');
        this.closeBtn.addEventListener('click', this.hide.bind(this));
    }

    private createBtn(title:string, id:string) {
        let btn = document.createElement("button");
        btn.innerHTML = title;
        btn.id = id;
        return btn;
    }

    showAddNewTask() {
        this.modalContent.innerHTML = 'New Task <br>';
        this.generateClearFormMarkup()
        this.modal.style.display = 'block';

        let btnsContainer = document.getElementById('btnsModalContainer');
        btnsContainer.innerHTML = '';

        this.saveTaskBtn = this.createBtn('Save', 'saveTask');
        btnsContainer.appendChild(this.saveTaskBtn);
        this.saveTaskBtn.addEventListener('click', this.addNewTask.bind(this));

        btnsContainer.appendChild(this.closeBtn);
    }

    showUpdateTask(task:Task) {
        this.modalContent.innerHTML = 'Update Task <br>';
        this.generateUpdateTaskForm(task);
        this.modal.style.display = 'block';

        let btnsContainer = document.getElementById('btnsModalContainer');
        btnsContainer.innerHTML = '';

        this.deleteTaskBtn = this.createBtn('Delete', 'deleteTask');
        this.deleteTaskBtn.addEventListener('click', () => this.deleteTask(task));
        btnsContainer.appendChild(this.deleteTaskBtn);

        this.updateTaskBtn = this.createBtn('Update', 'updateTask');
        this.updateTaskBtn.addEventListener('click', () => this.updateTask(task));
        btnsContainer.appendChild(this.updateTaskBtn);
        
        btnsContainer.appendChild(this.closeBtn);
    }

    private show() {
        this.modal.style.display = 'block';
    }

    private hide() {
        this.modal.style.display = 'none';
    }

    private addNewTask() {
        let fields = this.getValueOfFileds();
        let task = this.createTaskByFromFields(fields);
        this._dataService.addTask(task);
        this.hide();
    }

    private deleteTask(task:Task) {
        this._dataService.deleteTask(task);
        this.hide();
    }

    private updateTask(task:Task) {
        let fields = this.getValueOfFileds();
        let newTask = this.createTaskByFromFields(fields);
        newTask.id = task.id;
        console.log('old:', task, 'new: ',newTask);
        this._dataService.updateTask(newTask);
        this.hide();
    }

    private generateClearFormMarkup() {
        for(let i=0; i<this.formFields.length; i++) {
            switch(this.formFields[i].type) {
                case 'text': 
                    this.textInput(this.formFields[i].title, this.formFields[i].internalName, true);
                    break;
                case 'datepicker': 
                    this.datepickerInput(this.formFields[i]); 
                    break;
                case 'complete':
                    this.completeInput(this.formFields[i]);
                    break;
                case 'assignedTo':
                    this.assignedToInput(this.formFields[i]);
                    break;
                default:
                    console.log('такого поля нет') 
                    break;
            }           
        }
    }

    private generateUpdateTaskForm(task:Task) {
        for(let i=0; i<this.formFields.length; i++) {
            switch(this.formFields[i].type) {
                case 'text': 
                    this.textInput(this.formFields[i].title, this.formFields[i].internalName, true, task.title);
                    break;
                case 'datepicker': 
                    this.datepickerInput(this.formFields[i], task[this.formFields[i].internalName]); 
                    break;
                case 'complete':
                    this.completeInput(this.formFields[i], task.complete);
                    break;
                case 'assignedTo':
                    this.assignedToInput(this.formFields[i], task.assignedTo);
                    break;
                default:
                    console.log('такого поля нет') 
                    break;
            } 
        }
    }

    private textInput(title:string, id:string, required?:boolean, inputValue?:string) {
        let div = document.createElement("div");
        div.className = 'inputElement';
        let label = document.createElement("label");
        if (required) {
            title += '*';
        }
        label.innerText = title +': ';
        label.setAttribute('for', id);

        let input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.id = id;
        input.value = inputValue? inputValue : '';
 
        div.appendChild(label);
        div.appendChild(input);
        this.modalContent.appendChild(div);
    }


    private datepickerInput(formField:FormField, date?:Date) {
        if (date) {
            console.log("!!! formField.internalName: ", formField.internalName, 'date: ' ,date)
        }

        let div = document.createElement("div");
        div.className = 'inputElement';
        let label = document.createElement('label');
        label.innerText = formField.title + ': ';
        label.setAttribute('for', formField.internalName);

        let input = document.createElement('input');
        input.id = formField.internalName;

        div.appendChild(label);
        div.appendChild(input);
        this.modalContent.appendChild(div);

        let datepicker = Datepicker(input, {
            dateSelected: date? date : new Date(),
        });
    }

    assignedToInput(formField:FormField, defaultValue?:Resource) {
        console.log(' defaultValue assTo', defaultValue )

        let div = document.createElement("div");
        div.className = 'inputElement';
        let label = document.createElement('label');
        label.innerText = formField.title + ': ';

        //
        let select = document.createElement('select');
        select.id = formField.internalName;
        let opt1 = document.createElement('option');
        opt1.innerText = 'null';
        opt1.value = 'null';
        select.appendChild(opt1);
        let opt2 = document.createElement('option');
        opt2.innerText = 'employee1';
        opt2.value = 'employee1';
        select.appendChild(opt2);
        let opt3 = document.createElement('option');
        opt3.innerText = 'employee2';
        opt3.value = 'employee2';
        select.appendChild(opt3);
        //

        div.appendChild(label);
        div.appendChild(select);
        this.modalContent.appendChild(div);
    }

    private completeInput(formField: FormField, defaultValue?:number) {
        let div = document.createElement("div");
        div.className = 'inputElement';
        let label = document.createElement('label');
        label.setAttribute('for', formField.internalName);

        let input = document.createElement('input');
        input.id = formField.internalName;
        input.setAttribute('type', 'range');
        input.setAttribute('min', '0');
        input.setAttribute('max', '100');
        defaultValue ?  input.setAttribute('value', `${defaultValue}`) : input.setAttribute('value', '0');

        label.innerText = formField.title + ': ' + input.value;
        input.oninput = () => {
            label.innerHTML = formField.title + ': ' + input.value;
        }

        div.appendChild(label);
        div.appendChild(input);
        this.modalContent.appendChild(div);
    }



    private getValueOfFileds() {
        let fields = [];

        for(let i=0; i<this.modalContent.childNodes.length; i++) {
            let element = this.modalContent.childNodes[i] as HTMLElement;
            if ( element.tagName && element.tagName.toLowerCase() === 'div' ) {
                let c =this.getValueFromDiv(element);
                fields.push(c);
            }
        }
        return fields;
    }

    private getValueFromDiv(div:HTMLElement) {
        for (let i=0; i<div.childNodes.length; i++) {
            let element = div.childNodes[i] as HTMLElement;
            if (element.tagName && (element.tagName.toLowerCase() === 'input' || element.tagName.toLowerCase() === 'select')) {
                return { 
                    key: element.id,
                    value: (element as HTMLInputElement).value
                }; 
            } 
        }
    }

    private findValueForInternalName(key:string) {
        let elements = this.modalContent.getElementsByTagName('*');
        for(let i=0; i<elements.length; i++) {
            if ( elements[i].id === key ) {
                return (elements[i] as any).value;
            }
        }
    }

    private createTaskByFromFields(fields:any[]) {
        let task = new Task(null, '', null, null, new Resource(null, null), null);
        for(let i=0; i<this.formFields.length; i++) {
            let value = this.findValueForInternalName(this.formFields[i].internalName);
            if (value) {
                switch(this.formFields[i].type) {
                    case 'text':
                        break;
                    case 'datepicker':
                        value = new Date(value);
                        break;
                    case 'assignedTo':
                        value = { id: value, title: value }
                }
                task[this.formFields[i].internalName] = value;
            }
        }
        let id = Math.ceil(Math.random() * 10000) + Math.ceil(Math.random() * 100) * Math.ceil(Math.random() * 1000);
        task.id = 'new' + id;
        return task;
    }

    

    showAdvancedFilter() {
        this.modal.style.display = 'block';
        this.modalContent.innerHTML = ' ';
        this.modalContent.innerHTML = `
            <div>
                <span>Advanced Filter</span><br>
                <input type='text'>
                <span>Advanced Filter</span><br>
                <input type='text'>
                <span>Advanced Filter</span><br>
            </div>
        `;
        let btnsContainer = document.getElementById('btnsModalContainer');
        btnsContainer.innerHTML = '';
        btnsContainer.appendChild(this.closeBtn);
    }
}