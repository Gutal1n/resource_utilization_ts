export {}

declare global {
    interface Date {
        monthNames: string[];
        daysNames: string[];
        getDayName(): string;
        addDays(days: number): Date;
        getMonthName(): string ;
        getShortMonthName(): string ;
        getArrayDates(start:Date, end:Date): Date[];
        getArrayDatesWithUniqMonth(start:Date, end:Date): Date[];
    }    
}

Date.prototype.addDays = function(days: number) : Date {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

Date.prototype.daysNames = [
    "Sunday", "Monday", "Tuesday", "Wednesday",
    "Thursday", "Friday", "Saturday"
];

Date.prototype.getDayName = function() {
    return this.daysNames[this.getDay()];
};

Date.prototype.monthNames = [
    "January", "February", "March",
    "April", "May", "June",
    "July", "August", "September",
    "October", "November", "December"
];

Date.prototype.getMonthName = function() {
    return this.monthNames[this.getMonth()];
};
Date.prototype.getShortMonthName = function () {
    return this.getMonthName().substr(0, 3);
};

Date.prototype.getArrayDates = function(start:Date, end:Date) {
    let dateArray = new Array();
    let currentDate = start;
    while (currentDate <= end) {
        dateArray.push(new Date (currentDate.getTime()));
        currentDate = currentDate.addDays(1);
    }
    return dateArray;
}


Date.prototype.getArrayDatesWithUniqMonth = function(start:Date, end:Date) {
    let arrDates = new Array();
    let arrStr = new Array();
    let currentDate = start;
    while (currentDate <= end) {
        let d = currentDate.getMonthName() + currentDate.getFullYear();
        if ( arrStr.indexOf(d) === -1) {
            arrStr.push(d);
            arrDates.push(currentDate);
        }
        currentDate = currentDate.addDays(1);
    }
    return arrDates;
}
