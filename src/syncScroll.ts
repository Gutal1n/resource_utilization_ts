export default function syncScroll(firstContainer:string, secondContainer:string) {
    let isSyncingLeftScroll = false;
    let isSyncingRightScroll = false;
    let left = document.getElementById(firstContainer);
    let right = document.getElementById(secondContainer);

    left.addEventListener("scroll", function() {
        if (!isSyncingLeftScroll) {
            isSyncingRightScroll = true;
            right.scrollTop = this.scrollTop;
        }
        isSyncingLeftScroll = false;
    })

    right.addEventListener("scroll", function() {
        if (!isSyncingRightScroll) {
            isSyncingLeftScroll = true;
            left.scrollTop = this.scrollTop;
        }
        isSyncingRightScroll = false;
    })
}