export default class EventObserver {
    
    private _observers:any;

    constructor () {
        this._observers = []
    }

    // subscribe (fn:any) {
    //     this._observers.push(fn)
    // }

    // unsubscribe (fn:any) {
    //     this._observers = this._observers.filter( (subscriber:any) => subscriber !== fn)
    // }
    
    // broadcast (data:any) {
    //     this._observers.forEach( (subscriber:any) => subscriber(data) ) 
    // }

    subscribe (object:any) {
        this._observers.push(object)
    }

    unsubscribe (object:any) {
        this._observers = this._observers.filter( (subscriber:any) => subscriber !== object)
    }
    
    broadcast ( data:any ) {
        this._observers.forEach( (obj:any) => obj.update(data) ) 
    }

    
}
