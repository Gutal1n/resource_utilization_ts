import {Scale} from './model';
import './date.extension';

const DAY_MS = 86400000; //1000*60*60*24;

export default class Settings {
    private _scale:Scale;
    get scale(): Scale {
        return this._scale;
    }

    set scale(scale:Scale) {
        this._scale = scale;
        this.setStartAndEndDatesOfScale(this._selectedDate || new Date());
    }

    private _selectedDate: Date;
    get selectedDate(): Date {
        return this._selectedDate;
    }
    set selectedDate(date:Date) {
        this._selectedDate = date;
        this.setStartAndEndDatesOfScale(date);
    }

    startDate:Date;
    endDate:Date;

    widthCell: number;

    private _widthCanvas:number;
    get widthCanvas():number {
        this.calcWidthCanvas();
        return this._widthCanvas;
    }

    private _timePerPixel: number;
    get timePerPixel():number {
        this.calcutaleTimePerPixel();
        return this._timePerPixel;
    }   

    constructor() {
        this._scale = Scale.Days;
        this._selectedDate = new Date();
        this.setStartAndEndDatesOfScale(this.selectedDate);

        this.widthCell = 30;
        this.calcWidthCanvas();
        this.calcutaleTimePerPixel();
    }

    private setStartAndEndDatesOfScale(selectedDate: Date) {
        switch (this.scale) {
            case Scale.Hours:
                this.startDate = new Date(selectedDate.getTime()-DAY_MS*2);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS*2);
                break;
            case Scale.Days:
                this.startDate = new Date(selectedDate.getTime()-DAY_MS*40);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS*40);
                break; 
            case Scale.QDays:
                this.startDate = new Date(selectedDate.getTime()-DAY_MS*8);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS*8);
                break;
            case Scale.Weeks:
                this.startDate = new Date(selectedDate.getTime()-DAY_MS*112);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS*112);
                break;
            case Scale.ThirdsOfMonths:
                this.startDate = new Date(selectedDate.getTime()-DAY_MS*200);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS*200);
                break;
            case Scale.Months:
                this.startDate = new Date(selectedDate.getTime()-DAY_MS*650);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS*650);
                break;
            case Scale.Quarters:
                this.startDate = new Date(selectedDate.getTime()-DAY_MS*2000);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS*2000);
                break;
            case Scale.HalfYears:
                this.startDate = new Date(selectedDate.getTime()-DAY_MS*7300);
                this.endDate = new Date(selectedDate.getTime() + DAY_MS*7300);
                break;
        }
        this.startDate.setHours(0,0,0,0);
        this.endDate.setHours(23,59,59,999);
    }

    private calcWidthCanvas() {
        switch (this.scale) {
            case Scale.Hours:
                this._widthCanvas = this.widthCell * 24 * Date.prototype.getArrayDates(this.startDate, this.endDate).length;
                break;
            case Scale.Days:
                this._widthCanvas = this.widthCell * Date.prototype.getArrayDates(this.startDate, this.endDate).length;
                break;
            case Scale.QDays:
                this._widthCanvas = this.widthCell * 4 * Date.prototype.getArrayDates(this.startDate,this.endDate).length;
                break;
            case Scale.Weeks:
                this._widthCanvas = 2*this.widthCell * Math.ceil( Date.prototype.getArrayDates( this.startDate,this.endDate).length / 7 );
                break;
            case Scale.ThirdsOfMonths:
                this._widthCanvas = 2*this.widthCell  * Math.ceil( Date.prototype.getArrayDates(this.startDate,this.endDate).length / 10 );
                break;
            case Scale.Months:
                this._widthCanvas = 2*this.widthCell * Math.ceil( Date.prototype.getArrayDates(this.startDate,this.endDate).length / 30 );
                break;
            case Scale.Quarters:
                this._widthCanvas = 2*this.widthCell * Math.ceil( Date.prototype.getArrayDates(this.startDate,this.endDate).length / 90 );
                break;
            case Scale.HalfYears:
                this._widthCanvas = this.widthCell * Math.ceil( Date.prototype.getArrayDates(this.startDate,this.endDate).length / 180 );
                break;
        }
    }

    private calcutaleTimePerPixel() {
        this._timePerPixel = this.deltaTime(this.startDate, this.endDate) / this._widthCanvas;
    }

    private deltaTime(start:Date, end:Date): number {
        return end.getTime() - start.getTime();
    }

}

