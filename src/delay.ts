let delay = (function(){
    var timer = 0;
    return function(callback:any, ms:number){
      clearTimeout (timer);
      timer = setTimeout(callback, ms);
    };
  })();
export {delay};